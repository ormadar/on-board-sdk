//
// Created by ubuntu on 10/2/18.
//

#ifndef SERVER_WAYPOINTELEMENT_H
#define SERVER_WAYPOINTELEMENT_H

#include "IMissionElement.h"
#include "dji_linux_helpers.hpp"
#include "mission_sample.hpp"
#include "MissionCallbacks.h"


class WaypointElement : public IMissionElement {
public:

    WaypointElement(std::string type, DJI::OSDK::Vehicle *vehicle);

    virtual void print();

    virtual std::string getType();

    virtual void addPoint(pointStruct *ps);

    virtual void init();

    virtual DJI::OSDK::ACK::ErrorCode pause();

    virtual DJI::OSDK::ACK::ErrorCode resume();

    virtual DJI::OSDK::ACK::ErrorCode stop();

    virtual DJI::OSDK::ACK::ErrorCode build();

    virtual DJI::OSDK::ACK::ErrorCode startMission();

    void setWaypointEventCallback(DJI::OSDK::VehicleCallBack callback);

    void updateCurrentMission(int waypointIndex, int parameter);

    std::vector<DJI::OSDK::WayPointSettings> getWaypointSettings();

    DJI::OSDK::WayPointInitSettings getInitSettings();

    virtual std::vector<pointStruct> getPsVec();

    virtual std::string updateVelocity(float velocity);
//    void missionCallBack(DJI::OSDK::Vehicle *vehicle, DJI::OSDK::RecvContainer recvFrame,
//                         DJI::OSDK::UserData userData);

private:
    std::string type;
    std::vector<pointStruct> psVec;
    DJI::OSDK::WayPointInitSettings initSettings;
    std::vector<DJI::OSDK::WayPointSettings> wpVector;
    DJI::OSDK::Vehicle *vehicle;

    void setWaypointInitDefaults(int yawMode);

    void createWaypoints();

    void setWaypointDefaults(DJI::OSDK::WayPointSettings *wp, int yaw);

    DJI::OSDK::ACK::ErrorCode initStartSettings();

    void uploadWaypointMission();

};

#endif //SERVER_WAYPOINTELEMENT_H
