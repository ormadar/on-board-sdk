/*! @file mission_sample.cpp
 *  @version 3.3
 *  @date Jun 05 2017
 *
 *  @brief
 *  GPS Missions API usage in a Linux environment.
 *  Shows example usage of the Waypoint Missions and Hotpoint Missions through
 * the
 *  Mission Manager API.
 *
 *  @Copyright (c) 2017 DJI
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "mission_sample.hpp"
#include <sstream>
#include <thread>
#include <mutex>
#include <iomanip>

using namespace DJI::OSDK;
using namespace DJI::OSDK::Telemetry;
std::mutex vehicle_mutex;

bool _runTest = false;
bool _subscribed = false;

//Private Functions
bool isOnAir(DJI::OSDK::Vehicle *vehicle);

//End Of Private Functions
bool initSubscribeLocation(DJI::OSDK::Vehicle *vehicle, int responseTimeout) {
    if (_subscribed) {
        std::cout << "Already subcribed (:" << std::endl;
        sleep(1);
        return true;
    } else if (!vehicle->isM100() && !vehicle->isLegacyM600()) {
        if (!setUpSubscription(vehicle, responseTimeout)) {
            std::cout << "Failed to set up Subscription!" << std::endl;
            return false;
        }
        sleep(1);
        std::cout << "Subscription set sucessfully" << std::endl;
        return true;
    }
}

bool
setUpSubscription(DJI::OSDK::Vehicle *vehicle, int responseTimeout) {
    vehicle_mutex.lock();
    // Telemetry: Verify the subscription
    ACK::ErrorCode subscribeStatus;
    subscribeStatus = vehicle->subscribe->verify(responseTimeout);
    if (ACK::getError(subscribeStatus) != ACK::SUCCESS) {
        ACK::getErrorCodeMessage(subscribeStatus, __func__);
        return false;
    }
    vehicle_mutex.unlock();
    // Telemetry: Subscribe to flight status and mode at freq 10 Hz
    int freq = 10;
    TopicName topicList10Hz[] = {TOPIC_GPS_FUSED, TOPIC_BATTERY_INFO, TOPIC_VELOCITY, TOPIC_STATUS_FLIGHT, TOPIC_STATUS_DISPLAYMODE, TOPIC_QUATERNION};
    int numTopic = sizeof(topicList10Hz) / sizeof(topicList10Hz[0]);
    bool enableTimestamp = false;

    bool pkgStatus = vehicle->subscribe->initPackageFromTopicList(
            DEFAULT_PACKAGE_INDEX, numTopic, topicList10Hz, enableTimestamp, freq);
    if (!(pkgStatus)) {
        return pkgStatus;
        std::cout << __func__ << " returned false" << std::endl;
    }

    // Start listening to the telemetry data
    subscribeStatus =
            vehicle->subscribe->startPackage(DEFAULT_PACKAGE_INDEX, responseTimeout);
    if (ACK::getError(subscribeStatus) != ACK::SUCCESS) {
        ACK::getErrorCodeMessage(subscribeStatus, __func__);
        // Cleanup
        ACK::ErrorCode ack =
                vehicle->subscribe->removePackage(DEFAULT_PACKAGE_INDEX, responseTimeout);
        if (ACK::getError(ack)) {
            std::cout << "Error unsubscribing; please restart the drone/FC to get "
                         "back to a clean state.\n";
        } else {
            std::cout << "Successfully removed Start Package" << std::endl;
        }
        return false;
    }
    _subscribed = true;

    return true;
}

bool
teardownSubscription(DJI::OSDK::Vehicle *vehicle, const int pkgIndex,
                     int responseTimeout) {
    ACK::ErrorCode ack =
            vehicle->subscribe->removePackage(pkgIndex, responseTimeout);
    if (ACK::getError(ack)) {
        std::cout << "Error unsubscribing; please restart the drone/FC to get back "
                     "to a clean state.\n";
        return false;
    }
    _subscribed = false;
    return true;
}

std::string
runWaypointMission(Vehicle *vehicle, uint8_t numWaypoints, int responseTimeout,
                   std::vector <pointStruct> pointStructVector, bool runTest) {
    std::stringstream returnJson;
    //IF YOU WANT TEST MODE!, _runTest = true;
    _runTest = runTest;
    if (!_subscribed) //not subscribed todo return a string with not subscribed!!
        return nullptr;

    // Waypoint Mission : Initialization
    WayPointInitSettings fdata;
    setWaypointInitDefaults(&fdata);
    //Setting Crursing Speed Velocity , set by first v in the vector
    if (pointStructVector.size() != 0) {
        fdata.idleVelocity = pointStructVector[0].v; // todo stop crashing
    } else {
        // TEST MODE TODO: we must split it to 2 difrent function!!!!!
        fdata.idleVelocity = 10;
    }

    float64_t increment = 0.0004;
    float32_t start_alt = 10;
    //Check if is on air for exrta safe point (10m alt safe point)
    bool onAir = isOnAir(vehicle);
    if (!onAir)
        numWaypoints += 1;
    fdata.indexNumber = numWaypoints;
    ACK::ErrorCode initAck = vehicle->missionManager->init(
            DJI_MISSION_TYPE::WAYPOINT, responseTimeout, &fdata);
    if (ACK::getError(initAck)) {
        ACK::getErrorCodeMessage(initAck, __func__);
        returnJson << "{msg:Mission manager init error, data:" << initAck.data << "}";
        return returnJson.str();
    }

    vehicle->missionManager->printInfo();
    std::cout << "Initializing Waypoint Mission..\n";

    // Waypoint Mission: Create Waypoints

    std::vector <WayPointSettings> generatedWaypts = createWaypoints(vehicle, numWaypoints, increment, start_alt,
                                                                     pointStructVector, onAir);

    std::cout << "Creating Waypoints..\n";

    // Waypoint Mission: Upload the waypoints
    uploadWaypoints(vehicle, generatedWaypts, responseTimeout);
    std::cout << "Uploading Waypoints..\n";

    // Waypoint Mission: Start
    ACK::ErrorCode startAck =
            vehicle->missionManager->wpMission->start(responseTimeout);
    if (ACK::getError(startAck)) {
        ACK::getErrorCodeMessage(startAck, __func__);
        returnJson << "{msg:WayPointMission start error, data:" << startAck.data << "}";
        return returnJson.str();
    } else {
        std::cout << "Starting Waypoint Mission.\n";
    }
    returnJson << "{msg:runWayPoint mission success, data:" << 0 << "}";
    return returnJson.str();
}

Telemetry::TypeMap<TOPIC_GPS_FUSED>::type sampleData(Vehicle *vehicle) {

    // Global position retrieved via subscription
    Telemetry::TypeMap<TOPIC_GPS_FUSED>::type subscribeGPosition;
    subscribeGPosition = vehicle->subscribe->getValue<TOPIC_GPS_FUSED>();
//	  printf("Drone at (LLA): %f \t%f \t%f\n",
//			   subscribeGPosition.latitude, subscribeGPosition.longitude,
//			   subscribeGPosition.altitude);

    return subscribeGPosition;
}

int displayMode(Vehicle *vehicle){
    int subscribeDisplayMode;
    subscribeDisplayMode = vehicle->subscribe->getValue<TOPIC_STATUS_DISPLAYMODE>();
    return subscribeDisplayMode;
}

std::string sampleDataOld(Vehicle *vehicle) {
    // Global position retrieved via subscription
    Telemetry::TypeMap<TOPIC_GPS_FUSED>::type subscribeGPosition;
    // Global position retrieved via broadcast
    Telemetry::GlobalPosition broadcastGPosition;
    subscribeGPosition = vehicle->subscribe->getValue<TOPIC_GPS_FUSED>();
    std::stringstream jsonStream;
    jsonStream << "{lng: " << subscribeGPosition.longitude << ", lat: " << subscribeGPosition.latitude <<
               ", v: 12, alt:" << subscribeGPosition.altitude << "}";

    std::string returnJson = jsonStream.str();
//	  printf("Drone at (LLA): %f \t%f \t%f\n",
//			   subscribeGPosition.latitude, subscribeGPosition.longitude,
//			   subscribeGPosition.altitude);

    return returnJson;

}

void
setWaypointDefaults(WayPointSettings *wp) {
    wp->damping = 0;
    wp->yaw = 0;
    wp->gimbalPitch = 0;
    wp->turnMode = 0;
    wp->hasAction = 0;
    wp->actionTimeLimit = 100;
    wp->actionNumber = 0;
    wp->actionRepeat = 0;
    for (int i = 0; i < 16; ++i) {
        wp->commandList[i] = 0;
        wp->commandParameter[i] = 0;
    }
}

void
setWaypointInitDefaults(WayPointInitSettings *fdata) {
    fdata->maxVelocity = 15;
    fdata->idleVelocity = 5;
    fdata->finishAction = 0;
    fdata->executiveTimes = 1;
    fdata->yawMode = 0;
    fdata->traceMode = 0;
    fdata->RCLostAction = 1;
    fdata->gimbalPitch = 0;
    fdata->latitude = 0;
    fdata->longitude = 0;
    fdata->altitude = 0;
}

bool isOnAir(DJI::OSDK::Vehicle *vehicle) {
    //Check if already in air for safety measures !!
    //If not in air ,raise the drone to safe height before !
    Telemetry::TypeMap<TOPIC_STATUS_FLIGHT>::type flightStatus;
    flightStatus = vehicle->subscribe->getValue<TOPIC_STATUS_FLIGHT>();
    if (flightStatus == DJI::OSDK::VehicleStatus::FlightStatus::IN_AIR) {
        return true;
    } else {
        return false;
    }
}

std::vector <DJI::OSDK::WayPointSettings> createSafeWp(DJI::OSDK::Vehicle *vehicle, bool onAir) {
    std::vector <DJI::OSDK::WayPointSettings> wpVector;
    if (onAir)
        return wpVector;
    else {
        // Create Start Safe Waypoint
        WayPointSettings start_wp;
        setWaypointDefaults(&start_wp);
        // Global position retrieved via subscription
        Telemetry::TypeMap<TOPIC_GPS_FUSED>::type subscribeGPosition;
        subscribeGPosition = vehicle->subscribe->getValue<TOPIC_GPS_FUSED>();
        start_wp.latitude = subscribeGPosition.latitude;
        start_wp.longitude = subscribeGPosition.longitude;
        start_wp.altitude = 10;
        printf("Safe Waypoint created at (LLA): %f \t%f \t%f\n",
               start_wp.latitude, start_wp.longitude,
               10);
        start_wp.index = 0;
        wpVector.push_back(start_wp);
        return wpVector;

    }
}

std::vector <DJI::OSDK::WayPointSettings>
createWaypoints(DJI::OSDK::Vehicle *vehicle, int numWaypoints,
                float64_t distanceIncrement, float32_t start_alt, std::vector <pointStruct> pointStructVector,
                bool onAir) {
    //Create Safe point if needed

    std::vector <DJI::OSDK::WayPointSettings> wpVector;
    wpVector = createSafeWp(vehicle, onAir);
    if (_runTest) {
        wpVector = generateTestWpSetting(numWaypoints, wpVector, onAir);
    } else {
        //Console Input for wayPoints !!!!!
        //wpVector = generateUserWaypoints(&start_wp,distanceIncrement,numWaypoints);
        wpVector = convererToWpSettings(pointStructVector, wpVector, onAir);
    }
    //generateWaypointsPolygon(&start_wp, distanceIncrement, numWaypoints); thats was original example
    return wpVector;
}

std::vector <DJI::OSDK::WayPointSettings>
convererToWpSettings(std::vector <pointStruct> wpStructVector,
                     std::vector <DJI::OSDK::WayPointSettings> wpSettingVector,
                     bool onAir) {
    int b = 0;
    if (onAir)
        b = 0;
    else
        b = 1;
    int numOfWp = wpStructVector.size();
    for (int i = 0; i < numOfWp; i++) {

        WayPointSettings wp;
        setWaypointDefaults(&wp);
        wp.index = i + b;
        std::cout << "This is I:" << i << std::endl;
        wp.latitude = wpStructVector[i].lat;
        wp.longitude = wpStructVector[i].lng;
        wp.altitude = wpStructVector[i].alt;
        wpSettingVector.push_back(wp);
        std::cout << "lat : " << wpStructVector[i].lat << " lng:  " << wpStructVector[i].lng << " alt: "
                  << wpStructVector[i].alt << std::endl;

    }
    return wpSettingVector;

}

std::vector <DJI::OSDK::WayPointSettings>
generateUserWaypoints(WayPointSettings *start_data, float64_t increment, int numOfWaypoints) {
    std::vector <DJI::OSDK::WayPointSettings> wp_list;
    start_data->index = 0;
    wp_list.push_back(*start_data);

    std::cout << "Num of Wp is " << numOfWaypoints << std::endl;


    for (int i = 1; i < numOfWaypoints; i++) {
        float64_t lat, lon;
        float32_t alt;
        std::cout << "Enter lat lon alt " << std::endl;
        std::cin >> lat;
        std::cin >> lon;
        std::cin >> alt;
        WayPointSettings wp;
        WayPointSettings *prevWp = &wp_list[i - 1];
        setWaypointDefaults(&wp);
        wp.index = i;
        std::cout << "This is I:" << i << std::endl;
        wp.latitude = (lat);
        wp.longitude = (lon);
        wp.altitude = (alt);
        wp_list.push_back(wp);
        std::cout << "prev lat:" << prevWp->latitude << " long: " << prevWp->longitude << " alt :" << prevWp->altitude
                  << std::endl;
        std::cout << "current lat:" << wp.latitude << " long: " << wp.longitude << " alt :" << wp.altitude << std::endl;
        std::cout << "!!!!!!!!!!!!Index " << i << std::endl;

    }
//	  // Come back home
//      start_data->index = numOfWaypoints;
//      wp_list.push_back(*start_data);

    return wp_list;
}

std::vector <DJI::OSDK::WayPointSettings>
generateWaypointsPolygon(WayPointSettings *start_data, float64_t increment,
                         int num_wp) {

    // Let's create a vector to store our waypoints in.
    std::vector <DJI::OSDK::WayPointSettings> wp_list;

    // Some calculation for the polygon
    float64_t extAngle = 2 * M_PI / num_wp;

    // First waypoint
    start_data->index = 0;
    wp_list.push_back(*start_data);

    // Iterative algorithm
    for (int i = 1; i < num_wp; i++) {
        WayPointSettings wp;
        WayPointSettings *prevWp = &wp_list[i - 1];
        setWaypointDefaults(&wp);
        wp.index = i;
        wp.latitude = (prevWp->latitude + (increment * cos(i * extAngle)));
        wp.longitude = (prevWp->longitude + (increment * sin(i * extAngle)));
        wp.altitude = (prevWp->altitude + 1);
        wp_list.push_back(wp);
        std::cout << "prev lat:" << prevWp->latitude << " long: " << prevWp->longitude << " alt :" << prevWp->altitude
                  << std::endl;
        std::cout << "current lat:" << wp.latitude << " long: " << wp.longitude << " alt :" << wp.altitude << std::endl;
        std::cout << "!!!!!!!!!!!!Index " << i << std::endl;
    }

    // Come back home
    start_data->index = num_wp;
    wp_list.push_back(*start_data);

    return wp_list;
}

std::string
uploadWaypoints(Vehicle *vehicle, std::vector <DJI::OSDK::WayPointSettings> &wp_list, int responseTimeout) {
    std::stringstream returnJson;
    for (std::vector<WayPointSettings>::iterator wp = wp_list.begin();
         wp != wp_list.end(); ++wp) {
        printf("Waypoint created at (LLA): %f \t%f \t%f\n ", wp->latitude,
               wp->longitude, wp->altitude);
        ACK::WayPointIndex wpDataACK =
                vehicle->missionManager->wpMission->uploadIndexData(&(*wp),
                                                                    responseTimeout);
        if (ACK::getError(wpDataACK.ack)) {
            returnJson << "{msg:UploadIndexData error ,data:" << wpDataACK.ack.data;
            return returnJson.str();

        }
        ACK::getErrorCodeMessage(wpDataACK.ack, __func__);
    }
    returnJson << "{msg:UploadWayPoints success,data:" << 0 << "}";
    return returnJson.str();
}

bool
runHotpointMission(Vehicle *vehicle, int initialRadius, int responseTimeout) {
    if (!vehicle->isM100() && !vehicle->isLegacyM600()) {
        if (!setUpSubscription(vehicle, responseTimeout)) {
            std::cout << "Failed to set up Subscription!" << std::endl;
            return false;
        }
        sleep(1);
    }

    // Global position retrieved via subscription
    Telemetry::TypeMap<TOPIC_GPS_FUSED>::type subscribeGPosition;
    // Global position retrieved via broadcast
    Telemetry::GlobalPosition broadcastGPosition;

    // Hotpoint Mission Initialize
    vehicle->missionManager->init(DJI_MISSION_TYPE::HOTPOINT, responseTimeout,
                                  NULL);
    vehicle->missionManager->printInfo();

    if (!vehicle->isM100() && !vehicle->isLegacyM600()) {
        subscribeGPosition = vehicle->subscribe->getValue<TOPIC_GPS_FUSED>();
        vehicle->missionManager->hpMission->setHotPoint(
                subscribeGPosition.longitude, subscribeGPosition.latitude, initialRadius);
    } else {
        broadcastGPosition = vehicle->broadcast->getGlobalPosition();
        vehicle->missionManager->hpMission->setHotPoint(
                broadcastGPosition.longitude, broadcastGPosition.latitude, initialRadius);
    }

    // Takeoff
    ACK::ErrorCode takeoffAck = vehicle->control->takeoff(responseTimeout);
    if (ACK::getError(takeoffAck)) {
        ACK::getErrorCodeMessage(takeoffAck, __func__);

        if (!vehicle->isM100() && !vehicle->isLegacyM600()) {
            teardownSubscription(vehicle, DEFAULT_PACKAGE_INDEX, responseTimeout);
        }
        return false;
    } else {
        sleep(15);
    }

    // Start
    std::cout << "Start with default rotation rate: 15 deg/s" << std::endl;
    ACK::ErrorCode startAck =
            vehicle->missionManager->hpMission->start(responseTimeout);
    if (ACK::getError(startAck)) {
        ACK::getErrorCodeMessage(startAck, __func__);
        if (!vehicle->isM100() && !vehicle->isLegacyM600()) {
            teardownSubscription(vehicle, DEFAULT_PACKAGE_INDEX, responseTimeout);
        }
        return false;
    }
    sleep(20);

    // Pause
    std::cout << "Pause for 5s" << std::endl;
    ACK::ErrorCode pauseAck =
            vehicle->missionManager->hpMission->pause(responseTimeout);
    if (ACK::getError(pauseAck)) {
        ACK::getErrorCodeMessage(pauseAck, __func__);
    }
    sleep(5);

    // Resume
    std::cout << "Resume" << std::endl;
    ACK::ErrorCode resumeAck =
            vehicle->missionManager->hpMission->resume(responseTimeout);
    if (ACK::getError(resumeAck)) {
        ACK::getErrorCodeMessage(resumeAck, __func__);
    }
    sleep(10);

    // Update radius, no ACK
    std::cout << "Update radius to 1.5x: new radius = " << 1.5 * initialRadius
              << std::endl;
    vehicle->missionManager->hpMission->updateRadius(1.5 * initialRadius);
    sleep(10);

    // Update velocity (yawRate), no ACK
    std::cout << "Update hotpoint rotation rate: new rate = 5 deg/s" << std::endl;
    HotpointMission::YawRate yawRateStruct;
    yawRateStruct.clockwise = 1;
    yawRateStruct.yawRate = 5;
    vehicle->missionManager->hpMission->updateYawRate(yawRateStruct);
    sleep(10);

    // Stop
    std::cout << "Stop" << std::endl;
    ACK::ErrorCode stopAck =
            vehicle->missionManager->hpMission->stop(responseTimeout);

    std::cout << "land" << std::endl;
    ACK::ErrorCode landAck = vehicle->control->land(responseTimeout);
    if (ACK::getError(landAck)) {
        ACK::getErrorCodeMessage(landAck, __func__);
    } else {
        // No error. Wait for a few seconds to land
        sleep(10);
    }

    // Clean up
    ACK::getErrorCodeMessage(startAck, __func__);
    if (!vehicle->isM100() && !vehicle->isLegacyM600()) {
        teardownSubscription(vehicle, DEFAULT_PACKAGE_INDEX, responseTimeout);
    }

    return true;
}

std::vector <DJI::OSDK::WayPointSettings>
generateTestWpSetting(int numberOfWayPoints, std::vector <DJI::OSDK::WayPointSettings> wpVector, bool onAir) {
    int i = 0, b = 0;
    if (onAir) {
        i = 0;
        b = 0;
    } else {
        i = 1;
        b = 1;
    }

    for (; i < numberOfWayPoints; i++) {
        WayPointSettings wp;
        WayPointSettings *prevWp = &wpVector[i - b];
        setWaypointDefaults(&wp);
        double a = 0.000001;
        wp.index = i;
        wp.longitude = (prevWp->longitude + a);
        wp.altitude = (prevWp->altitude + 1);
        wp.latitude = (prevWp->latitude + a); //1.988958  0.393446
        wpVector.push_back(wp);
        std::cout << "prev lat:" << std::setprecision(9) << prevWp->latitude << " long: " << std::setprecision(9)
                  << prevWp->longitude << " alt :" << prevWp->altitude << std::endl;
        std::cout << "current lat:" << wp.latitude << " long: " << wp.longitude << " alt :" << wp.altitude << std::endl;
        std::cout << "!!!!!!!!!!!!Index " << i << std::endl;
    }
    return wpVector;
}

void setTest() {
    _runTest = true;
    std::cout << "inside Test " << std::endl;
}

std::string resumeWayPointMission(DJI::OSDK::Vehicle *vehicle, int responseTimeout) {
    std::stringstream returnJson;
    std::cout << "Resuming Mission" << std::endl;
    ACK::ErrorCode resumeAck = vehicle->missionManager->wpMission->resume(responseTimeout);
    if (ACK::getError(resumeAck)) {
        ACK::getErrorCodeMessage(resumeAck, __func__);
        returnJson << "{msg:ResumeWayPointMission error,data:" << resumeAck.data << "}";
        return returnJson.str();
    }
    returnJson << "{msg:ResumeWayPointMission success,data:" << 0 << "}";
    return returnJson.str();
}

std::string pauseWayPointMission(DJI::OSDK::Vehicle *vehicle, int responseTimeout) {
    std::stringstream returnJson;
    std::cout << "Pausing Mission..." << std::endl;
    ACK::ErrorCode pauseAck = vehicle->missionManager->wpMission->pause(responseTimeout);

    if (ACK::getError(pauseAck)) {
        ACK::getErrorCodeMessage(pauseAck, __func__);
        returnJson << "{msg:pauseWayPointMission error,data:" << pauseAck.data;
        return returnJson.str();
    }
    returnJson << "{msg:pauseWayPointMission success,data:" << 0 << "}";
    return returnJson.str();
}

std::string stopWaypointMission(DJI::OSDK::Vehicle *vehicle, int responseTimeout) {
    std::stringstream returnJson;
    // Stop
    std::cout << "Stop" << std::endl;
    ACK::ErrorCode stopAck =
            vehicle->missionManager->wpMission->stop(responseTimeout);
    if (ACK::getError(stopAck)) {
        ACK::getErrorCodeMessage(stopAck, __func__);
        returnJson << "{msg:stopWayPointMission error,data:" << stopAck.data << "}";
        return returnJson.str();
    }

    std::cout << "Mission stopped " << std::endl;
    returnJson << "{msg:stopWayPointMission success,data:" << 0 << "}";
    return returnJson.str();


}

bool saveHomeLocation(Vehicle *vehicle, DJI::OSDK::WayPointSettings *homeLocation) {

    // Create Start Waypoint

    setWaypointDefaults(&(*homeLocation));

    // Global position retrieved via subscription
    Telemetry::TypeMap<TOPIC_GPS_FUSED>::type subscribeGPosition;
    // Global position retrieved via broadcast
    Telemetry::GlobalPosition broadcastGPosition;


    subscribeGPosition = vehicle->subscribe->getValue<TOPIC_GPS_FUSED>();
    homeLocation->latitude = subscribeGPosition.latitude;
//    homeLocation->latitude = 32.1045;
    homeLocation->longitude = subscribeGPosition.longitude;
//    homeLocation->longitude = 34.8044;
    homeLocation->altitude = subscribeGPosition.altitude;
    printf("HomePoint location created at (LLA): %f \t%f \t%f\n",
           homeLocation->latitude, homeLocation->longitude,
           homeLocation->altitude);

    return true;
}

std::string
returnHomePoint(DJI::OSDK::Vehicle *vehicle, int responseTimeout, DJI::OSDK::WayPointSettings *homeLocation) {
    std::stringstream returnJson;
    std::vector <DJI::OSDK::WayPointSettings> homeRoute;
    WayPointInitSettings fdata;
    setWaypointInitDefaults(&fdata);
    fdata.finishAction = 2; //Auto landing
    fdata.indexNumber = 2;
    std::cout << "Return Home Function" << std::endl;
    WayPointSettings firstStep;
    WayPointSettings secondeStep;
    setWaypointDefaults(&firstStep);
    setWaypointDefaults(&secondeStep);


    //First Step go 30 meters in same location
    Telemetry::TypeMap<TOPIC_GPS_FUSED>::type subscribeGPosition;
    subscribeGPosition = vehicle->subscribe->getValue<TOPIC_GPS_FUSED>();
    firstStep.latitude = subscribeGPosition.latitude;
    firstStep.longitude = subscribeGPosition.longitude;
    firstStep.altitude = 30;
    firstStep.index = 0;
    printf("HomeRoute 1 created at (LLA): %f \t%f \t%f\n",
           firstStep.latitude, firstStep.longitude, 30);

    homeRoute.push_back(firstStep);
    //Seconde go to home location ,the auto landing is perfomed
    secondeStep.latitude = homeLocation->latitude;
    secondeStep.longitude = homeLocation->longitude;
    secondeStep.altitude = 30;
    secondeStep.index = 1;
    printf("HomeRoute 2  created at (LLA): %f \t%f \t%f\n",
           firstStep.latitude, firstStep.longitude,
           30);
    homeRoute.push_back(secondeStep);


    ACK::ErrorCode initAck = vehicle->missionManager->init(
            DJI_MISSION_TYPE::WAYPOINT, responseTimeout, &fdata);

    if (ACK::getError(initAck)) {
        ACK::getErrorCodeMessage(initAck, __func__);
        returnJson << "{msg:ErrorReturnHome initFunc,data:" << initAck.data << "}";
        return returnJson.str();

    }

    vehicle->missionManager->printInfo();
    std::cout << "Initializing Return Home Mission..\n";

    // Waypoint Mission: Upload the waypoints
    uploadWaypoints(vehicle, homeRoute, responseTimeout);
    std::cout << "Uploading Home location..\n";

    // Waypoint Mission: Start
    ACK::ErrorCode startAck =
            vehicle->missionManager->wpMission->start(responseTimeout);
    if (ACK::getError(startAck)) {
        ACK::getErrorCodeMessage(startAck, __func__);
        std::cout << __func__ << std::endl;
        std::cout << "We got an error" << std::endl;
        returnJson << "{msg:ErrorReturnHome wpMission error,data:" << startAck.data << "}";
        return returnJson.str();
    } else {
        std::cout << "Starting Waypoint Mission.\n";
    }
    returnJson << "{msg:ReturnHome success,data:" << 0 << "}";
    return returnJson.str();

}

Telemetry::TypeMap<TOPIC_QUATERNION>::type getHeadingInfo(Vehicle* vehicle )
{

    // Global position retrieved via subscription
    Telemetry::TypeMap<TOPIC_QUATERNION>::type subscribeGPosition;
    subscribeGPosition = vehicle->subscribe->getValue<TOPIC_QUATERNION>();

//	  printf("Drone at (LLA): %f \t%f \t%f\n",
//			   subscribeGPosition.latitude, subscribeGPosition.longitude,
//			   subscribeGPosition.altitude);

    return subscribeGPosition;

}


Telemetry::TypeMap<TOPIC_BATTERY_INFO>::type getBatteryInfo(DJI::OSDK::Vehicle *vehicle) {
    Telemetry::TypeMap<TOPIC_BATTERY_INFO>::type subscribeBatteryInfo;
    subscribeBatteryInfo = vehicle->subscribe->getValue<TOPIC_BATTERY_INFO>();
    //std::cout << "Battery Precentage : " << (unsigned)subscribeBatteryInfo.percentage << std::endl;
    return subscribeBatteryInfo;
}

std::string updateVelocity(DJI::OSDK::Vehicle *vehicle, float velocity, int responseTimeout) {
    std::stringstream returnJson;
    ACK::WayPointVelocity ackVelocity;
    ackVelocity = vehicle->missionManager->wpMission->updateIdleVelocity(velocity, responseTimeout);
    if (ACK::getError(ackVelocity.ack)) {
        returnJson << "{msg:updateVelocity error,data:" << ackVelocity.ack.data << "}";

        ACK::getErrorCodeMessage(ackVelocity.ack, __func__);
        return returnJson.str();
    }
    returnJson << "{msg:updateVelocity success,data:" << 0 << "}";
    return returnJson.str();
}

Telemetry::TypeMap<TOPIC_VELOCITY>::type getVelocity(DJI::OSDK::Vehicle *vehicle) {
    // Velocity retrieved via subscription
    Telemetry::TypeMap<TOPIC_VELOCITY>::type subscribeVelocity;
    subscribeVelocity = vehicle->subscribe->getValue<TOPIC_VELOCITY>();
    return subscribeVelocity;
}

std::string retrunToHomeTest(DJI::OSDK::Vehicle *vehicle, int responseTimeout) {
//	std::stringstream returnJson;
//	ACK::ErrorCode controlAck = vehicle->control->goHome(responseTimeout);
//	if(ACK::getErrorCodeMessage(controlAck))
//	{
//		returnJson << "{msg:returnHome error,data:"<<controlAck.data << "}";
//		return returnJson.str();
//	}
//	returnJson << "{msg:returnHome success,data:"<<0 <<"}";
    return "roi z";

}

std::string rcReturnHomePoint(DJI::OSDK::Vehicle *vehicle, int responseTimeout) {
    std::stringstream returnJson;
    ACK::ErrorCode homeAck = vehicle->control->goHome(responseTimeout);
    if (ACK::getError(homeAck)) {
        ACK::getErrorCodeMessage(homeAck, __func__);
        returnJson << "{msg:RemoteController return homePoint error,data:" << homeAck.data << "}";
        return returnJson.str();
    }
    returnJson << "{msg:RemoteController return HomePoint success!!,data:0}";
    return returnJson.str();
}

std::string landNowMission(DJI::OSDK::Vehicle *vehicle, int responseTimeout) {
    std::stringstream returnJson;
    ACK::ErrorCode landAck = vehicle->control->land(responseTimeout);
    if (ACK::getError(landAck)) {
        ACK::getErrorCodeMessage(landAck, __func__);
        returnJson << "{msg:RemoteController Land Now error,data:" << landAck.data << "}";
        return returnJson.str();
    }

    returnJson << "{msg:RemoteController return HomePoint success!!,data:0}";
    return returnJson.str();
}



