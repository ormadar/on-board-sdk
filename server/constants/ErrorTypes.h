//
// Created by ubuntu on 10/17/18.
//

#ifndef SERVER_ERRORTYPES_H
#define SERVER_ERRORTYPES_H

#include <string>

namespace ErrorTypes {

    std::string getErrorById(int id);

};


#endif //SERVER_ERRORTYPES_H
