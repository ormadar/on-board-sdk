//
// Created by ubuntu on 10/17/18.
//

#include "ErrorTypes.h"

namespace ErrorTypes {


    std::string getErrorById(int id) {
        switch (id) {
            case 0:
                return "Success";
            case 1:
                return "Wrong waypoint index";
            case 160:
                return "Too near to home";
            case 161:
                return "Too close to home, within 20 meters";
            case 162:
                return "Invalid hotpoint parameter";
            case 163:
                return "Invalid latitude or longtitude";
            case 166:
                return "Invalid direction";
            case 169:
                return "Hotpoint paused";
            case 170:
                return "Hotpoint failed to pause";
            case 176:
                return "Too far from your position, lack of radio connection";
            case 177:
                return "Cutoff time overflow";
            case 178:
                return "Gimbal pitch angle too large";
            case 192:
                return "Too high";
            case 193:
                return "Too low";
            case 194:
                return "Invalid radius";
            case 195:
                return "Yaw rate too large";
            case 196:
                return "Invalid vision";
            case 197:
                return "Invalid yaw mode";
            case 198:
                return "Too far from Hotpoint";
            case 199:
                return "Too far from home";
            case 200:
                return "Mission not supported";
            case 201:
                return "Too far from current position";
            case 202:
                return "Beginner mode does not support missions";
            case 208:
                return "Not at mode F";
            case 209:
                return "Need obtain control";
            case 210:
                return "Need close IOC mode";
            case 211:
                return "Mission not initialized";
            case 212:
                return "Mission not running";
            case 213:
                return "Mission already running";
            case 214:
                return "Too consuming of time";
            case 215:
                return "Other mission running";
            case 216:
                return "Bad GPS";
            case 217:
                return "Low battery";
            case 218:
                return "UAV did not take off";
            case 219:
                return "Wrong parameters";
            case 220:
                return "Conditions not satisfied";
            case 221:
                return "Crossing No-Fly zone";
            case 222:
                return "Unrecorded home";
            case 223:
                return "Already at No-Fly zone";
            case 224:
                return "Invalid waypoint mission data";
            case 225:
                return "Invalid waypoint point data";
            case 226:
                return "Waypoint distance out of range";
            case 227:
                return "Waypoint mission out of range";
            case 228:
                return "Too many points";
            case 229:
                return "Points too close";
            case 230:
                return "Points too far";
            case 231:
                return "Check failed";
            case 232:
                return "Invalid action";
            case 233:
                return "Point data not enough";
            case 234:
                return "Waypoint mission data not enough";
            case 235:
                return "Waypoints not enough";
            case 236:
                return "Waypoint mission already running";
            case 237:
                return "Waypoint mission is not running";
            case 238:
                return "Invalid velocity";
            case 240:
                return "Taking off";
            case 241:
                return "Landing";
            case 242:
                return "Returning home";
            case 243:
                return "Starting motors";
            case 244:
                return "Invalid command";
            case 255:
                return "Unknown error";
            default:
                return "Unknown error";
        }
    }
}