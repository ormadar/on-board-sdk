//
// Created by ubuntu on 10/2/18.
//

#ifndef SERVER_PARSER_H
#define SERVER_PARSER_H

#include <string>
#include "mission_sample.hpp"
#include "IMissionElement.h"
#include "WaypointElement.h"
#include "HotpointElement.h"
#include "constants/MissionTypes.h"

namespace Parser {

    std::vector<IMissionElement *> convert(std::vector<pointStruct> psVec, DJI::OSDK::Vehicle* vehicle);

    IMissionElement *getElementByType(pointStruct *ps, DJI::OSDK::Vehicle* vehicle);
};


#endif //SERVER_PARSER_H
