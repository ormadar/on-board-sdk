#include "mission_sample.hpp"
#include "Parser.h"
#include "MissionManager.h"
#include <tgmath.h>
#include <math.h>
#include <algorithm>
#include <pistache/http.h>
#include <pistache/router.h>
#include <pistache/endpoint.h>
#include <json/json.h>
#include <json/reader.h>
#include <json/writer.h>
#include <json/value.h>
#include "testboos.hpp"
#include "constants/ErrorTypes.h"
#include "CallbackHandler.h"
#include <exception>
#include <thread>

using namespace std;
using namespace Pistache;
using namespace DJI::OSDK;
using namespace DJI::OSDK::Telemetry;


class StatsEndpoint {
public:
    Vehicle *_vehicle;

    bool validateDroneNo(int responseTimeout) {
        ACK::DroneVersion droneVersion = _vehicle->getDroneVersion(responseTimeout);
        char *serialNo = "0670136229";

        if (strcmp(droneVersion.data.hw_serial_num, serialNo) != 0)
            return false;
        else
            return true;

    }

    StatsEndpoint(Address addr) : httpEndpoint(std::make_shared<Http::Endpoint>(addr)) {}

    bool init(char *projectPath, size_t thr = 2) {

        //!!!!!!!!!Initizalize of Vehicle
        // Initialize variables
        int functionTimeout = 1;

        // Setup OSDK.
        std::cout << "This is project path : " << projectPath << std::endl;
        LinuxSetup *linuxEnvironment = new LinuxSetup(1, &projectPath);
        _vehicle = linuxEnvironment->getVehicle();
        if (!validateDroneNo(1))
            exit(0);
        if (_vehicle == NULL) {
            std::cout << "Vehicle not initialized, exiting.\n";
            return -1;
        }

        // Obtain Control Authority
        _vehicle->obtainCtrlAuthority(functionTimeout);
        int responseTimeout = 1;
        if (!initSubscribeLocation(_vehicle, responseTimeout)) {
            std::cout << "cannot subscribe!!! exiting./n" << std::endl;
            return -1;
        }
        //!!!!!!!!!!!End Initizalize of vehicle
        //Saving home location !!!!
        sleep(1);
        _homeLocation = new WayPointSettings();
        saveHomeLocation(_vehicle, _homeLocation);
        std::cout << "Manifold is ready	 !!!!" << std::endl;
        auto opts = Http::Endpoint::options().threads(thr).flags(
                Tcp::Options::InstallSignalHandler | Tcp::Options::ReuseAddr);
        std::cout << "Server is ready lets get started !!!!!!" << std::endl;
        try {
            httpEndpoint->init(opts);
            setupRoutes();
        }
        catch (std::runtime_error e) {
            //Try again maybe will work
            std::cout << "THis the problem" << std::endl;
            throw (e);
        }


        return true;
    }


    void start() {
        httpEndpoint->setHandler(router.handler());
        httpEndpoint->serve();
    }

    void shutdown() {
        httpEndpoint->shutdown();
    }

    void sampTest() {
        std::cout << "inside samp Test " << std::endl;
        //Vehicle *v = _linuxEnvironment->getVehicle();
        std::cout << "NewVV!!!!!!!" << std::endl;
        sampleData(_vehicle);
    }

private:

    WayPointSettings *_homeLocation;
    const int _responseTimeOut = 1;

    void setupRoutes() {
        using namespace Rest;
        Routes::Post(router, "/setCourse", Routes::bind(&StatsEndpoint::startMission, this));
        Routes::Post(router, "/setUpdatedCourse", Routes::bind(&StatsEndpoint::startUpdatedMission, this));
        Routes::Get(router, "/testMission", Routes::bind(&StatsEndpoint::testMission, this));
        Routes::Get(router, "/pauseMission", Routes::bind(&StatsEndpoint::pauseMission, this));
        Routes::Get(router, "/resumeMission", Routes::bind(&StatsEndpoint::resumeMission, this));
        Routes::Get(router, "/connect", Routes::bind(&StatsEndpoint::getInfo, this));
        Routes::Get(router, "/stopMission", Routes::bind(&StatsEndpoint::stopMission, this));
        Routes::Get(router, "/saveHomePoint", Routes::bind(&StatsEndpoint::saveHomePoint, this));
        Routes::Get(router, "/returnHome", Routes::bind(&StatsEndpoint::returnHome, this));
        Routes::Get(router, "/obtainControl", Routes::bind(&StatsEndpoint::obtainControlAuthority, this));
        Routes::Get(router, "/updateVelocity/:v", Routes::bind(&StatsEndpoint::updateV, this));
        Routes::Get(router, "/remoteControllerHome/", Routes::bind(&StatsEndpoint::rcHome, this));
        Routes::Get(router, "/landNow", Routes::bind(&StatsEndpoint::landNow, this));
    }

    //#################DJI functions ################################
    void rcHome(const Rest::Request &request, Http::ResponseWriter response) {
        std::cout << "Inside RcHome Func" << std::endl;
        std::string json;
        json = rcReturnHomePoint(_vehicle, _responseTimeOut);
        response.send(Http::Code::Ok, json);
    }

    void updateV(const Rest::Request &request, Http::ResponseWriter response) {
        std::cout << "Inside updateVelocity Func" << std::endl;
        std::string responseMsg;
        MissionManagerRef *missionManager;
        missionManager = missionManager->getInstance(&_vehicle);

        if (request.hasParam(":v")) {
            std::string strVelocity;
            auto requestVelocity = request.param(":v");
            strVelocity = requestVelocity.as<std::string>();
            std::string a = strVelocity.substr(strVelocity.find("=") + 1);

            float velocity;
            std::string::size_type sz;
            velocity = std::stod(a, &sz);
            std::cout << "Velocity recived " << velocity << std::endl;
            std::string json = missionManager->updateVelocity(velocity);
            response.send(Http::Code::Ok, json);

        }

    }

    void obtainControlAuthority(const Rest::Request &request, Http::ResponseWriter response) {
        std::cout << "Inside ObtainControlAuthority Func" << std::endl;
        std::string responseMsg = "Obtaing Control Authority recived by server";
        //response.send(Http::Code::Ok, responseMsg);
        ACK::ErrorCode obtainAck = _vehicle->obtainCtrlAuthority(_responseTimeOut);
        std::stringstream returnJson;
        if (ACK::getError(obtainAck)) {
            returnJson << "{msg:Error obtain Control,data:" << obtainAck.data << "}";

        } else {
            returnJson << "{msg:Obtain Control Success,data:" << 0 << "}";
        }
        response.send(Http::Code::Ok, returnJson.str());

    }

    void returnHome(const Rest::Request &request, Http::ResponseWriter response) {
        std::cout << "Inside ReturnHome Func" << std::endl;
        std::string responseMsg = "return home request recived by server ";

        std::string json = returnHomePoint(_vehicle, 50, _homeLocation);
        response.send(Http::Code::Ok, json);
        //originalReturnHome(_vehicle,1);
    }

    void saveHomePoint(const Rest::Request &request, Http::ResponseWriter response) {
        std::cout << "Inside saveHomeLocation #1" << std::endl;
        std::string responseMsg = "saveHomeLocation request recived by server ";
        std::cout << "Inside saveHomeLocation #2" << std::endl;
        saveHomeLocation(_vehicle, _homeLocation);
        std::cout << "Inside saveHomeLocation #3" << std::endl;
        response.send(Http::Code::Ok, responseMsg);
        std::cout << "Inside saveHomeLocation #4" << std::endl;
    }

    void testMission(const Rest::Request &request, Http::ResponseWriter response) {
        //Guard guard(vehicleLock);
        std::cout << "!@!@!@!@!@! Inside TestMission Handle !!" << std::endl;
        std::string responseMsg = ""; //TODO check if its ok for you;
        // setTest();
        std::cout << "Change test " << std::endl;
        //For test struct is created null
        std::vector<pointStruct> nullStruct;
        responseMsg = runWaypointMission(_vehicle, 4, 50, nullStruct, true);
        response.send(Http::Code::Ok, responseMsg);


    }

    float getHeadDirection(Telemetry::TypeMap<TOPIC_QUATERNION>::type quaternion_yaw) {
        float w;
        float headDirection;

        w = quaternion_yaw.q0;
        headDirection = 2 * acos(w) * 180 / M_PI;

        if (quaternion_yaw.q3 < 0) {
            headDirection *= -1;
        }
        return headDirection;

    }

    void getInfo(const Rest::Request &request, Http::ResponseWriter response) {
        //std::cout << "Inside getInfo func " << std::endl;
        Telemetry::TypeMap<TOPIC_GPS_FUSED>::type gpsInfo;
        Telemetry::TypeMap<TOPIC_BATTERY_INFO>::type batteryInfo;
        Telemetry::TypeMap<TOPIC_VELOCITY>::type velocityInfo;
        Telemetry::TypeMap<TOPIC_HEIGHT_FUSION>::type heightInfo;
        Telemetry::TypeMap<TOPIC_QUATERNION>::type quaternion_yaw;

        gpsInfo = sampleData(_vehicle);
        batteryInfo = getBatteryInfo(_vehicle);
        velocityInfo = getVelocity(_vehicle);
        quaternion_yaw = getHeadingInfo(_vehicle);

        float headDirection = getHeadDirection(quaternion_yaw);

        //TODO add json builder func or use the jsopCpp api
        std::stringstream jsonStream;
        jsonStream <<
                   "{lng: " << gpsInfo.longitude <<
                   ", lat: " << gpsInfo.latitude <<
                   ", vX: " << velocityInfo.data.x <<
                   ", vY: " << velocityInfo.data.y <<
                   ", vZ:" << velocityInfo.data.z <<
                   ", alt:" << gpsInfo.altitude <<
                   ", heading:" << headDirection <<
                   ", battery:" << (unsigned) batteryInfo.percentage <<
                   "}";
        std::string json = jsonStream.str();
        response.send(Http::Code::Ok, json);
    }

    void pauseMission(const Rest::Request &request, Http::ResponseWriter response) {
        ACK::ErrorCode stopAck;
        std::string responseMsg = "Pause Mission request recived by the server";
        MissionManagerRef *missionManager;
        missionManager = missionManager->getInstance(&_vehicle);
        stopAck = missionManager->pauseMission();
        response.send(Http::Code::Ok, ErrorTypes::getErrorById(stopAck.data));
    }

    void resumeMission(const Rest::Request &request, Http::ResponseWriter response) {
        ACK::ErrorCode stopAck;
        std::string responseMsg = "Resume Mission request recived by the server";
        MissionManagerRef *missionManager;
        missionManager = missionManager->getInstance(&_vehicle);
        stopAck = missionManager->resumeMission();
        response.send(Http::Code::Ok, ErrorTypes::getErrorById(stopAck.data));
    }

    void landNow(const Rest::Request &request, Http::ResponseWriter response) {
        std::string reponseMsg = "Land Now request recived by the server";
        std::string json = landNowMission(_vehicle, 50);
        response.send(Http::Code::Ok, json);
    }

    void stopMission(const Rest::Request &request, Http::ResponseWriter response) {
        ACK::ErrorCode stopAck;
        std::string responseMsg = "Stop Mission request recived by the server";
        MissionManagerRef *missionManager;
        missionManager = missionManager->getInstance(&_vehicle);
        stopAck = missionManager->stopMission();
        response.send(Http::Code::Ok, ErrorTypes::getErrorById(stopAck.data));
    }

    void startMissionRoute(const Rest::Request &request, Http::ResponseWriter &response, bool isUpdated = false) {
        //std::string responseMsg = "Start Mission request recived by the server";
        std::string *returnJson;
        MissionManagerRef *missionManager;
        missionManager = missionManager->getInstance(&_vehicle);

        //response.send(Http::Code::Ok, responseMsg);
        std::string json = request.body();
        Json::Value root;
        Json::Reader reader;
        std::vector<pointStruct> psVec;
        bool parsingSuccessfull = reader.parse(json.c_str(), root);
        if (!parsingSuccessfull) {
            std::cout << "Faild to parse " << std::endl;
            return;
        }

        try {

            pointStruct *ps = new pointStruct();
            std::string::size_type sz;

            std::string lat;
            std::string lng;
            std::string alt;
            std::string v;
            std::string radius;
            std::string type;
            std::string headingType;
            std::string heading;
            std::vector<pointStruct> psVec;

            int numOfPoint = root["points"].size();

            for (int i = 0; i < numOfPoint; i++) {
                lat = root["points"][i].get("lat", "def").asString();
                lng = root["points"][i].get("lng", "def").asString();
                alt = root["points"][i].get("alt", "def").asString();
                v = root["points"][i].get("v", "def").asString();
                radius = root["points"][i].get("radius", "def").asString();
                type = root["points"][i].get("missionType", "def").asString();
                headingType = root["points"][i].get("headingType", "def").asString();
                heading = root["points"][i].get("heading", "def").asString();

                ps->lat = std::stof(lat, &sz) * M_PI / 180;
                ps->lng = std::stof(lng, &sz) * M_PI / 180;
                ps->alt = std::stod(alt, &sz);
                ps->v = std::stod(v, &sz);
                ps->radius = std::stod(radius, &sz);
                ps->type = type;
                ps->headingType = headingType;
                ps->heading = std::stoi(heading, &sz);
                psVec.push_back(*ps);

                //TODO: make logs instead
                std::cout << "point-" << i << "lat: " << ps->lat << " alt: " << ps->alt << "lng :" << ps->lng << " v: "
                          << v << " type: " << type << " heading: " << heading << std::endl;
            }
            if (psVec[0].type == "waypoint") {
                if(!isUpdated) {
                    Telemetry::TypeMap<TOPIC_GPS_FUSED>::type gpsInfo;
                    gpsInfo = sampleData(_vehicle);
                    float lastPointHeight = psVec[numOfPoint - 1].alt;
                    ps->lat = gpsInfo.latitude;
                    ps->lng = gpsInfo.longitude;
                    ps->alt = 10;
                    ps->v = 10;
                    ps->radius = 10;
                    ps->type = "waypoint";
                    ps->headingType = psVec[0].headingType;
                    ps->heading = psVec[0].heading;
                    if(lastPointHeight > 10){
                        ps->alt = lastPointHeight;
                    }
                    psVec.push_back(*ps);
                } else {
                    IMissionElement* element = missionManager->getRunningElement(); // last point
                    if(!element){
                        response.send(Http::Code::Not_Acceptable, "You can't update a mission without any mission running");
                        return;
                    }
                    std::vector<pointStruct> realPsVec = element->getPsVec();
                    int numOfPoints = realPsVec.size() - 1;
                    ps->lat = realPsVec[numOfPoints].lat;
                    ps->lng = realPsVec[numOfPoints].lng;
                    ps->v = realPsVec[numOfPoints].v;
                    ps->radius = realPsVec[numOfPoints].radius;
                    ps->type = "waypoint";
                    ps->headingType = realPsVec[numOfPoints].headingType;
                    ps->heading = realPsVec[numOfPoints].heading;
                    ps->alt = realPsVec[numOfPoints].alt;
                    psVec.push_back(*ps);
                    Telemetry::TypeMap<TOPIC_GPS_FUSED>::type gpsInfo; // first point
                    gpsInfo = sampleData(_vehicle);
                    ps->lat = gpsInfo.latitude;
                    ps->lng = gpsInfo.longitude;
                    ps->alt = psVec[0].alt;
                    ps->v = psVec[0].v;
                    ps->radius = psVec[0].radius;
                    ps->type = "waypoint";
                    ps->headingType = psVec[0].headingType;
                    ps->heading = psVec[0].heading;
                    psVec.insert(psVec.begin(), *ps);
                }
            }

            std::vector<IMissionElement *> elementVec = Parser::convert(psVec, _vehicle);

            //      For Debugging!!!!!
            for (int j = 0; j < elementVec.size(); j++) {
                std::cout << "The element number: " << j << " The element type: " << elementVec.at(j)->getType()
                          << "The points are: " << std::endl;
                elementVec.at(j)->print();
            }
            missionManager->setElementVector(elementVec);
            DJI::OSDK::ACK::ErrorCode ackError;
            ackError = missionManager->startMission();
            if (ACK::getError(ackError) && ackError.data != 0) {
                response.send(Http::Code::Not_Acceptable, ErrorTypes::getErrorById(ackError.data));
                return;
            }
            response.send(Http::Code::Ok, "Mission started successfully!!");



//            setWaypointEventCallback(waypointEventCallback, 0);


            //NUMOFPOINTS = jsonArray +1  Remember
//            returnJson = runWaypointMission(_vehicle, numOfPoint, 50, psVec, false);
//            response.send(Http::Code::Ok, returnJson);

        }
        catch (std::exception &e) {
            std::cout << e.what() << std::endl;
            response.send(Http::Code::Ok,
                          "problem with the Server"); //todo: check that Roy i think status should be other.
        }
    }


    void startMission(const Rest::Request &request, Http::ResponseWriter response) {
        // const Rest::Request &request, Http::ResponseWriter response
        startMissionRoute(request, response, false);
    }

    void startUpdatedMission(const Rest::Request &request, Http::ResponseWriter response) {
        startMissionRoute(request, response, true);
    }

    //END OF DJI FUNCTION ###########################################
    std::shared_ptr<Http::Endpoint> httpEndpoint;

    Rest::Router router;
};



int main(int argc, char *argv[]) {
//    DJI::OSDK::Log::instance().enableDebugLogging();
    std::cout << "New version" << std::endl;
    Port port(9081);
    int thr = 2;

    if (argc >= 2) {
        port = std::stol(argv[1]);

        if (argc == 3)
            thr = std::stol(argv[2]);
    }

    Address addr(Ipv4::any(), port);

    cout << "Cores = " << hardware_concurrency() << endl;
    cout << "Using " << thr << " threads" << endl;
    cout << argv[0] << endl;
    StatsEndpoint stats(addr);
    std::cout << "before init" << std::endl;
    if (!stats.init(argv[0], thr)) {
        std::cout << "Faild To init " << std::endl;
        return -1;
    }
    std::cout << "after init" << std::endl;
//    CallbackHandler::settingUpThread(&stats._vehicle);

    stats.start();
    stats.shutdown();
}

