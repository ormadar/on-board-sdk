//
// Created by ubuntu on 10/23/18.
//

#ifndef SERVER_CALLBACKHANDLER_H
#define SERVER_CALLBACKHANDLER_H
#include <iostream>
#include <thread>
#include "mission_sample.hpp"


namespace CallbackHandler {

    void handleCallback();

    void callbackDemoThread();

    void settingUpThread(DJI::OSDK::Vehicle **vehicle);

    void setEstimatedTime(float time);

    void runTimerForHotPointMission();

};


#endif //SERVER_CALLBACKHANDLER_H
