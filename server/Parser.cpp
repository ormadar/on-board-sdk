//
// Created by ubuntu on 10/2/18.
//

#include "Parser.h"
#include <json/json.h>
#include <json/reader.h>
#include <json/writer.h>
#include <json/value.h>
#include <iostream>
#include <fstream>

namespace Parser {

    std::vector<IMissionElement *> convert(std::vector<pointStruct> psVec, DJI::OSDK::Vehicle *vehicle) {
        std::vector<IMissionElement *> elementVec;
        pointStruct *ps = new pointStruct();
        *ps = psVec.at(0);          // Handeling first point

        IMissionElement *element = getElementByType(ps, vehicle);
        element->addPoint(ps);

        std::string currentType = ps->type;

        for (int i = 1; i < psVec.size(); i++) {
            *ps = psVec.at(i);
            currentType = ps->type;
            // If the current point is still type waypoint
            if (currentType == Waypoint && currentType == element->getType()) {
                element->addPoint(ps);
            } else {
                // If the point is hotpoint or waypoint before a hotpoint
                element->init();
                elementVec.push_back(element);
                element = getElementByType(ps, vehicle);
                element->addPoint(ps);
            }
        }
        // Handeling last element
        element->init();
        elementVec.push_back(element);

//      For Debugging!!!!!
//        for (int j = 0; j < elementVec.size(); j++) {
//            std::cout << "The element number: " << j << " The element type: " << elementVec.at(j)->getType()
//                      << "The points are: " << std::endl;
//            elementVec.at(j)->print();
//        }

        return elementVec;
    }


    IMissionElement *getElementByType(pointStruct *ps, DJI::OSDK::Vehicle *vehicle) {
        std::string waypoint = "waypoint";
        if (ps->type == waypoint) {
            return new WaypointElement(ps->type, vehicle);
        } else {
            return new HotpointElement(ps->type, vehicle);
        }
    }

}



