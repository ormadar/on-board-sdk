//
// Created by ubuntu on 10/3/18.
//

#include "IMissionElement.h"
#include <iostream>

IMissionElement::IMissionElement() {
    std::cout << "IMissionElement" << std::endl;
}

void IMissionElement::addPoint(pointStruct *ps) {
    std::cout << "add point IMissionElement " << std::endl;
}

void IMissionElement::print() {
    std::cout << "print IMissionElement" << std::endl;
}

void IMissionElement::init() {
    std::cout << "Init has been called IMissionElement" << std::endl;
}

DJI::OSDK::ACK::ErrorCode IMissionElement::pause() {
    std::cout << "IMissionElement mission paused" << std::endl;
}

DJI::OSDK::ACK::ErrorCode IMissionElement::resume() {
    std::cout << "IMissionElement mission resumed" << std::endl;
}

DJI::OSDK::ACK::ErrorCode IMissionElement::stop() {
    std::cout << "IMissionElement mission stopped" << std::endl;
}

DJI::OSDK::ACK::ErrorCode IMissionElement::build() {
    std::cout << "IMissionElement mission build" << std::endl;
}

DJI::OSDK::ACK::ErrorCode IMissionElement::startMission() {
    std::cout << "IMissionElement mission start" << std::endl;
}

std::string IMissionElement::updateVelocity(float velocity){
    std::cout << "IMissionElement update velocity" << std::endl;
}

std::vector<pointStruct> IMissionElement::getPsVec(){
    std::cout << "IMissionElement get psVec" << std::endl;
}



