//
// Created by ubuntu on 10/10/18.
//

#ifndef SERVER_MISSIONCALLBACKS_H
#define SERVER_MISSIONCALLBACKS_H


#include <dji_vehicle_callback.hpp>

using namespace DJI::OSDK;

namespace MissionCallbacks {
    void onUpdate(Vehicle *vehicle, RecvContainer recvFrame, UserData userData);

};


#endif //SERVER_MISSIONCALLBACKS_H
