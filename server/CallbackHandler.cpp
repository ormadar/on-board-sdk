//
// Created by ubuntu on 10/23/18.
//

#include "CallbackHandler.h"
#include <unistd.h>
#include <string>
#include <chrono>
#include <mutex>

using namespace std::chrono;

static int prevMode;
static int currentMode;
static std::mutex vehicle_mutex;
static DJI::OSDK::Vehicle *vehicleTemp;
static bool availableToCheckTime;
static float estimatedTime = 0;
static int currentTime = 0;

static DJI::OSDK::Vehicle *vehicle;


namespace CallbackHandler {

    void handleCallback() {
        currentMode = displayMode(vehicleTemp);
        if((currentMode != 14 && currentMode != 9) && (prevMode == 14 || prevMode == 9)){
            // ready to start another mission
        } else{
            prevMode = currentMode;
        }
    }

    void callbackDemoThread() {
        DJI::OSDK::ACK::ErrorCode stopAck;
        availableToCheckTime = false;
        while (true) {
            handleCallback();
            sleep(1);
            if(availableToCheckTime){
                currentTime++;
                if(currentTime > estimatedTime){
                    vehicle_mutex.lock();
                    stopAck = vehicleTemp->missionManager->hpMission->stop(10); // TODO:: Fix back
                    vehicle_mutex.unlock();
                    availableToCheckTime = false;
                }
            }
        }
    }

    void settingUpThread(DJI::OSDK::Vehicle **vehicle) {
        prevMode = 0;
        currentMode = 0;
        vehicleTemp = *vehicle;
        std::thread callbackThread(callbackDemoThread);
        callbackThread.detach();
    }

    void setEstimatedTime(float time) {
        estimatedTime = time;
    }

    void runTimerForHotPointMission() {
        currentTime = 0;
        availableToCheckTime = true;
    }

}
