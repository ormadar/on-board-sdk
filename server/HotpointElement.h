//
// Created by ubuntu on 10/3/18.
//

#ifndef SERVER_HOTPOINTELEMENT_H
#define SERVER_HOTPOINTELEMENT_H

#include "IMissionElement.h"
#include "CallbackHandler.h"


class HotpointElement : public IMissionElement {
public:

    HotpointElement(std::string type, DJI::OSDK::Vehicle *vehicle);

    virtual void print();

    virtual std::string getType();

    virtual void addPoint(pointStruct *ps);

    virtual void init();

    virtual DJI::OSDK::ACK::ErrorCode pause();

    virtual DJI::OSDK::ACK::ErrorCode resume();

    virtual DJI::OSDK::ACK::ErrorCode stop();

    virtual DJI::OSDK::ACK::ErrorCode build();

    virtual DJI::OSDK::ACK::ErrorCode startMission();

    virtual std::string updateVelocity(float velocity);

    virtual std::vector<pointStruct> getPsVec();


//    static void hotpointCallback(DJI::OSDK::Vehicle *vehicle, DJI::OSDK::RecvContainer recvFrame,
//                               DJI::OSDK::UserData userData);

private:

    DJI::OSDK::Vehicle *vehicle;
    std::string type;
    std::vector<pointStruct> psVec;

    void setInitSettings();

};


#endif //SERVER_HOTPOINTELEMENT_H
