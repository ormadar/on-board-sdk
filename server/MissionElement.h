//
// Created by ubuntu on 10/2/18.
//

#ifndef SERVER_MISSIONELEMENT_H
#define SERVER_MISSIONELEMENT_H


#include <string>
#include "mission_sample.hpp"

class MissionElement {
public:

//    virtual ~MissionElement();

    virtual std::string getType() = 0;

    virtual void addPoint(pointStruct *ps) = 0;

//    virtual MissionElement getElement();

    virtual DJI::OSDK::ACK::ErrorCode build() = 0;

//    virtual void setInitSettings();

    virtual void init() = 0;

    virtual DJI::OSDK::ACK::ErrorCode pause() = 0;

    virtual DJI::OSDK::ACK::ErrorCode resume() = 0;

    virtual DJI::OSDK::ACK::ErrorCode stop() = 0;

    virtual DJI::OSDK::ACK::ErrorCode startMission() = 0;

    virtual std::string updateVelocity(float velocity) = 0;

//    virtual void setCallbacks();
};

#endif //SERVER_MISSIONELEMENT_H
