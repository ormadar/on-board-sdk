//
// Created by ubuntu on 10/10/18.
//

#include "MissionCallbacks.h"
#include <iostream>

void MissionCallbacks::onUpdate(Vehicle *vehicle, RecvContainer recvFrame, UserData userData) {
    std::cout << "MissionCallbacks called" << std::endl;
}