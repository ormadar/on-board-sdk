//
// Created by ubuntu on 10/3/18.
//

#include "HotpointElement.h"
#include <iostream>
#include "mission_sample.hpp"
#include <dji_vehicle_callback.hpp>
#include "dji_linux_helpers.hpp"


int responseTimeOutHotPoint = 100;

void HotpointElement::print() {

    if (this->psVec.size()) {
        for (int i = 0; i < this->psVec.size(); i++) {
            std::cout << "point number: " << i << "lat: " << psVec.at(i).lat << " alt: " << psVec.at(i).alt
                      << " lng :" << psVec.at(i).lng << " v: "
                      << psVec.at(i).v << " type: " << psVec.at(i).type << std::endl;
        }
    }

}

DJI::OSDK::ACK::ErrorCode HotpointElement::startMission() {
    DJI::OSDK::HotpointMission::YawRate yawRateStruct;
    float estimatedTime;
    yawRateStruct.clockwise = 1;
//    yawRateStruct.yawRate = this->psVec[0].v / this->psVec[0].radius;
    yawRateStruct.yawRate = this->psVec[0].v;
    std::cout << "Yaw rate is: " << this->psVec[0].v << std::endl;

    /*calculating time it should take to complete 1 lap -- pi is approx ~~ 3.142*/

    estimatedTime = (2 * 3.142 * this->psVec[0].radius) / (yawRateStruct.yawRate);
    DJI::OSDK::ACK::ErrorCode ackError;
    std::cout << "Starting Hotpoint Mission" << std::endl;

    int flightStatus;
    flightStatus = vehicle->subscribe->getValue<TOPIC_STATUS_FLIGHT>();
    if (!(flightStatus == 2)) {
        this->vehicle->control->takeoff(responseTimeOutHotPoint);
        sleep(5);
    }
    ackError = this->vehicle->missionManager->hpMission->start(responseTimeOutHotPoint);
    if (DJI::OSDK::ACK::getError(ackError) && ackError.data != 0) {
        std::cout << "Start error" << ackError.data << std::endl;
    } else {
        this->vehicle->missionManager->hpMission->updateYawRate(yawRateStruct);
        std::cout << "Start Good" << ackError.data << std::endl;

    }
    return ackError;
}

DJI::OSDK::ACK::ErrorCode HotpointElement::build() {
    DJI::OSDK::ACK::ErrorCode ackError;
    std::cout << "Building Hotpoint Mission" << std::endl;
    ackError = this->vehicle->missionManager->init(DJI::OSDK::DJI_MISSION_TYPE::HOTPOINT, responseTimeOutHotPoint,
                                                   NULL);
    this->vehicle->missionManager->printInfo();
    setInitSettings();
    if (DJI::OSDK::ACK::getError(ackError) && ackError.data != 0) {
        std::cout << "start settings error" << std::endl;
    } else {
        std::cout << "start settings Good" << std::endl;
    }
    return ackError;
}

void HotpointElement::setInitSettings() {
    // Setting coordinates
    this->vehicle->missionManager->hpMission->setHotPoint(this->psVec[0].lng, this->psVec[0].lat, this->psVec[0].alt);
    // Setting radius
    this->vehicle->missionManager->hpMission->setRadius(this->psVec[0].radius);
    // Setting head direction
    if (this->psVec[0].headingType == "face_inside_mode") {
        this->vehicle->missionManager->hpMission->setYawMode(DJI::OSDK::HotpointMission::YawMode::YAW_INSIDE);
    } else {
        this->vehicle->missionManager->hpMission->setYawMode(DJI::OSDK::HotpointMission::YawMode::YAW_AUTO);
    }
}

DJI::OSDK::ACK::ErrorCode HotpointElement::pause() {
    DJI::OSDK::ACK::ErrorCode ackError;
    ackError = this->vehicle->missionManager->hpMission->pause(responseTimeOutHotPoint);
    return ackError;
}

DJI::OSDK::ACK::ErrorCode HotpointElement::resume() {
    DJI::OSDK::ACK::ErrorCode ackError;
    ackError = this->vehicle->missionManager->hpMission->resume(responseTimeOutHotPoint);
    return ackError;
}

DJI::OSDK::ACK::ErrorCode HotpointElement::stop() {
    DJI::OSDK::ACK::ErrorCode ackError;
    ackError = this->vehicle->missionManager->hpMission->stop(responseTimeOutHotPoint);
    return ackError;
}

HotpointElement::HotpointElement(std::string type, DJI::OSDK::Vehicle *vehicle) {
    this->type = type;
    this->vehicle = vehicle;
}

std::string HotpointElement::getType() {
    return this->type;

}

void HotpointElement::addPoint(pointStruct *ps) {
    this->psVec.push_back(*ps);
}

void HotpointElement::init() {
    std::cout << "init HotpointElement" << std::endl;
}

std::string HotpointElement::updateVelocity(float velocity) {
    DJI::OSDK::HotpointMission::YawRate yawRateStruct;
    std::stringstream returnJson;
    DJI::OSDK::ACK::ErrorCode ackError;
    std::cout << "Inside update velocity HOTPOINT" << std::endl;
    yawRateStruct.clockwise = 1;
    yawRateStruct.yawRate = velocity;
    this->vehicle->missionManager->hpMission->updateYawRate(yawRateStruct);
    returnJson << "{msg:updateVelocity for hotpoint mission" << "}";
    return returnJson.str();
}

std::vector<pointStruct> HotpointElement::getPsVec() {
    return this->psVec;
}
