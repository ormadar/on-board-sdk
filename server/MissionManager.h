//
// Created by ubuntu on 10/7/18.
//

#ifndef SERVER_MISSIONMANAGER_H
#define SERVER_MISSIONMANAGER_H

#include "IMissionElement.h"
#include "testboos.hpp"
#include <dji_vehicle.hpp>
#include "WaypointElement.h"


class MissionManagerRef {

public:

    static MissionManagerRef *getInstance(DJI::OSDK::Vehicle **pvehicle);

    void setElementVector(std::vector<IMissionElement *> elementVector);

    DJI::OSDK::ACK::ErrorCode pauseMission();

    DJI::OSDK::ACK::ErrorCode resumeMission();

    DJI::OSDK::ACK::ErrorCode stopMission();

    DJI::OSDK::ACK::ErrorCode startMission();

    std::string updateVelocity(float velocity);

    IMissionElement* getRunningElement();

private:
    int currentMissionIndex = 0;
    DJI::OSDK::Vehicle *vehicle;
    std::vector<IMissionElement *> elementVec;
    static MissionManagerRef *_missionManager;

    MissionManagerRef &operator=(const MissionManagerRef &) { return *this; };

    MissionManagerRef(DJI::OSDK::Vehicle **pvehicle);

};


#endif //SERVER_MISSIONMANAGER_H
