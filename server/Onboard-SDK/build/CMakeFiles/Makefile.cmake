# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

# The generator used is:
SET(CMAKE_DEPENDS_GENERATOR "Unix Makefiles")

# The top level Makefile was generated from the following files:
SET(CMAKE_MAKEFILE_DEPENDS
  "CMakeCache.txt"
  "../CMakeLists.txt"
  "CMakeFiles/2.8.12.2/CMakeCCompiler.cmake"
  "CMakeFiles/2.8.12.2/CMakeCXXCompiler.cmake"
  "CMakeFiles/2.8.12.2/CMakeSystem.cmake"
  "../contrib/DJIConfig.cmake"
  "../osdk-core/CMakeLists.txt"
  "../osdk-core/cmake-modules/DJIOSDKConfig.cmake.in"
  "../osdk-core/cmake-modules/DJIOSDKConfigVersion.cmake.in"
  "../sample/linux/CMakeLists.txt"
  "../sample/linux/camera-gimbal/CMakeLists.txt"
  "../sample/linux/flight-control/CMakeLists.txt"
  "../sample/linux/logging/CMakeLists.txt"
  "../sample/linux/mfio/CMakeLists.txt"
  "../sample/linux/missions/CMakeLists.txt"
  "../sample/linux/mobile/CMakeLists.txt"
  "../sample/linux/telemetry/CMakeLists.txt"
  "/usr/share/cmake-2.8/Modules/CMakeCCompiler.cmake.in"
  "/usr/share/cmake-2.8/Modules/CMakeCCompilerABI.c"
  "/usr/share/cmake-2.8/Modules/CMakeCInformation.cmake"
  "/usr/share/cmake-2.8/Modules/CMakeCXXCompiler.cmake.in"
  "/usr/share/cmake-2.8/Modules/CMakeCXXCompilerABI.cpp"
  "/usr/share/cmake-2.8/Modules/CMakeCXXInformation.cmake"
  "/usr/share/cmake-2.8/Modules/CMakeClDeps.cmake"
  "/usr/share/cmake-2.8/Modules/CMakeCommonLanguageInclude.cmake"
  "/usr/share/cmake-2.8/Modules/CMakeDetermineCCompiler.cmake"
  "/usr/share/cmake-2.8/Modules/CMakeDetermineCXXCompiler.cmake"
  "/usr/share/cmake-2.8/Modules/CMakeDetermineCompiler.cmake"
  "/usr/share/cmake-2.8/Modules/CMakeDetermineCompilerABI.cmake"
  "/usr/share/cmake-2.8/Modules/CMakeDetermineCompilerId.cmake"
  "/usr/share/cmake-2.8/Modules/CMakeDetermineSystem.cmake"
  "/usr/share/cmake-2.8/Modules/CMakeFindBinUtils.cmake"
  "/usr/share/cmake-2.8/Modules/CMakeGenericSystem.cmake"
  "/usr/share/cmake-2.8/Modules/CMakeParseImplicitLinkInfo.cmake"
  "/usr/share/cmake-2.8/Modules/CMakeSystem.cmake.in"
  "/usr/share/cmake-2.8/Modules/CMakeSystemSpecificInformation.cmake"
  "/usr/share/cmake-2.8/Modules/CMakeTestCCompiler.cmake"
  "/usr/share/cmake-2.8/Modules/CMakeTestCXXCompiler.cmake"
  "/usr/share/cmake-2.8/Modules/CMakeTestCompilerCommon.cmake"
  "/usr/share/cmake-2.8/Modules/CMakeUnixFindMake.cmake"
  "/usr/share/cmake-2.8/Modules/Compiler/GNU-C.cmake"
  "/usr/share/cmake-2.8/Modules/Compiler/GNU-CXX.cmake"
  "/usr/share/cmake-2.8/Modules/Compiler/GNU.cmake"
  "/usr/share/cmake-2.8/Modules/MultiArchCross.cmake"
  "/usr/share/cmake-2.8/Modules/Platform/Linux-CXX.cmake"
  "/usr/share/cmake-2.8/Modules/Platform/Linux-GNU-C.cmake"
  "/usr/share/cmake-2.8/Modules/Platform/Linux-GNU-CXX.cmake"
  "/usr/share/cmake-2.8/Modules/Platform/Linux-GNU.cmake"
  "/usr/share/cmake-2.8/Modules/Platform/Linux.cmake"
  "/usr/share/cmake-2.8/Modules/Platform/UnixPaths.cmake"
  )

# The corresponding makefile is:
SET(CMAKE_MAKEFILE_OUTPUTS
  "Makefile"
  "CMakeFiles/cmake.check_cache"
  )

# Byproducts of CMake generate step:
SET(CMAKE_MAKEFILE_PRODUCTS
  "CMakeFiles/2.8.12.2/CMakeSystem.cmake"
  "CMakeFiles/2.8.12.2/CMakeCCompiler.cmake"
  "CMakeFiles/2.8.12.2/CMakeCXXCompiler.cmake"
  "CMakeFiles/2.8.12.2/CMakeCCompiler.cmake"
  "CMakeFiles/2.8.12.2/CMakeCXXCompiler.cmake"
  "CMakeFiles/CMakeDirectoryInformation.cmake"
  "osdk-core/CMakeFiles/CMakeDirectoryInformation.cmake"
  "sample/linux/CMakeFiles/CMakeDirectoryInformation.cmake"
  "sample/linux/camera-gimbal/CMakeFiles/CMakeDirectoryInformation.cmake"
  "sample/linux/flight-control/CMakeFiles/CMakeDirectoryInformation.cmake"
  "sample/linux/mfio/CMakeFiles/CMakeDirectoryInformation.cmake"
  "sample/linux/missions/CMakeFiles/CMakeDirectoryInformation.cmake"
  "sample/linux/mobile/CMakeFiles/CMakeDirectoryInformation.cmake"
  "sample/linux/telemetry/CMakeFiles/CMakeDirectoryInformation.cmake"
  "sample/linux/logging/CMakeFiles/CMakeDirectoryInformation.cmake"
  )

# Dependency information for all targets:
SET(CMAKE_DEPEND_INFO_FILES
  "osdk-core/CMakeFiles/djiosdk-core.dir/DependInfo.cmake"
  "sample/linux/camera-gimbal/CMakeFiles/djiosdk-cameragimbal-sample.dir/DependInfo.cmake"
  "sample/linux/flight-control/CMakeFiles/djiosdk-flightcontrol-sample.dir/DependInfo.cmake"
  "sample/linux/mfio/CMakeFiles/djiosdk-mfio-sample.dir/DependInfo.cmake"
  "sample/linux/missions/CMakeFiles/djiosdk-mission-sample.dir/DependInfo.cmake"
  "sample/linux/mobile/CMakeFiles/djiosdk-mobile-sample.dir/DependInfo.cmake"
  "sample/linux/telemetry/CMakeFiles/djiosdk-telemetry-sample.dir/DependInfo.cmake"
  "sample/linux/logging/CMakeFiles/djiosdk-logging-sample.dir/DependInfo.cmake"
  )
