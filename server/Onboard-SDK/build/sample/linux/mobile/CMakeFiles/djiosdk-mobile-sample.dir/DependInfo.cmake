# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/ubuntu/Documents/dji-sdk/Onboard-SDK/sample/linux/camera-gimbal/camera_gimbal_sample.cpp" "/home/ubuntu/Documents/dji-sdk/Onboard-SDK/build/sample/linux/mobile/CMakeFiles/djiosdk-mobile-sample.dir/__/camera-gimbal/camera_gimbal_sample.cpp.o"
  "/home/ubuntu/Documents/dji-sdk/Onboard-SDK/sample/linux/common/dji_linux_environment.cpp" "/home/ubuntu/Documents/dji-sdk/Onboard-SDK/build/sample/linux/mobile/CMakeFiles/djiosdk-mobile-sample.dir/__/common/dji_linux_environment.cpp.o"
  "/home/ubuntu/Documents/dji-sdk/Onboard-SDK/sample/linux/common/dji_linux_helpers.cpp" "/home/ubuntu/Documents/dji-sdk/Onboard-SDK/build/sample/linux/mobile/CMakeFiles/djiosdk-mobile-sample.dir/__/common/dji_linux_helpers.cpp.o"
  "/home/ubuntu/Documents/dji-sdk/Onboard-SDK/sample/linux/flight-control/flight_control_sample.cpp" "/home/ubuntu/Documents/dji-sdk/Onboard-SDK/build/sample/linux/mobile/CMakeFiles/djiosdk-mobile-sample.dir/__/flight-control/flight_control_sample.cpp.o"
  "/home/ubuntu/Documents/dji-sdk/Onboard-SDK/sample/linux/missions/mission_sample.cpp" "/home/ubuntu/Documents/dji-sdk/Onboard-SDK/build/sample/linux/mobile/CMakeFiles/djiosdk-mobile-sample.dir/__/missions/mission_sample.cpp.o"
  "/home/ubuntu/Documents/dji-sdk/Onboard-SDK/sample/linux/mobile/main.cpp" "/home/ubuntu/Documents/dji-sdk/Onboard-SDK/build/sample/linux/mobile/CMakeFiles/djiosdk-mobile-sample.dir/main.cpp.o"
  "/home/ubuntu/Documents/dji-sdk/Onboard-SDK/sample/linux/mobile/mobile_sample.cpp" "/home/ubuntu/Documents/dji-sdk/Onboard-SDK/build/sample/linux/mobile/CMakeFiles/djiosdk-mobile-sample.dir/mobile_sample.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/ubuntu/Documents/dji-sdk/Onboard-SDK/build/osdk-core/CMakeFiles/djiosdk-core.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "../sample/linux/../../osdk-core/api/inc"
  "../sample/linux/../../osdk-core/utility/inc"
  "../sample/linux/../../osdk-core/hal/inc"
  "../sample/linux/../../osdk-core/protocol/inc"
  "../sample/linux/../../osdk-core/platform/linux/inc"
  "../sample/linux/mobile/../common"
  "../sample/linux/mobile/../flight-control"
  "../sample/linux/mobile/../camera-gimbal"
  "../sample/linux/mobile/../missions"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
