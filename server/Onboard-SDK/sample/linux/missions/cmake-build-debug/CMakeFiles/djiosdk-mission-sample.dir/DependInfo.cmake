# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/ubuntu/Desktop/onboard-sdk-test/on-board-test/server/Onboard-SDK/sample/linux/missions/Callback.cpp" "/home/ubuntu/Desktop/onboard-sdk-test/on-board-test/server/Onboard-SDK/sample/linux/missions/cmake-build-debug/CMakeFiles/djiosdk-mission-sample.dir/Callback.cpp.o"
  "/home/ubuntu/Desktop/onboard-sdk-test/on-board-test/server/Onboard-SDK/sample/linux/common/dji_linux_environment.cpp" "/home/ubuntu/Desktop/onboard-sdk-test/on-board-test/server/Onboard-SDK/sample/linux/missions/cmake-build-debug/CMakeFiles/djiosdk-mission-sample.dir/home/ubuntu/Desktop/onboard-sdk-test/on-board-test/server/Onboard-SDK/sample/linux/common/dji_linux_environment.cpp.o"
  "/home/ubuntu/Desktop/onboard-sdk-test/on-board-test/server/Onboard-SDK/sample/linux/common/dji_linux_helpers.cpp" "/home/ubuntu/Desktop/onboard-sdk-test/on-board-test/server/Onboard-SDK/sample/linux/missions/cmake-build-debug/CMakeFiles/djiosdk-mission-sample.dir/home/ubuntu/Desktop/onboard-sdk-test/on-board-test/server/Onboard-SDK/sample/linux/common/dji_linux_helpers.cpp.o"
  "/home/ubuntu/Desktop/onboard-sdk-test/on-board-test/server/Onboard-SDK/sample/linux/missions/main.cpp" "/home/ubuntu/Desktop/onboard-sdk-test/on-board-test/server/Onboard-SDK/sample/linux/missions/cmake-build-debug/CMakeFiles/djiosdk-mission-sample.dir/main.cpp.o"
  "/home/ubuntu/Desktop/onboard-sdk-test/on-board-test/server/Onboard-SDK/sample/linux/missions/mission_sample.cpp" "/home/ubuntu/Desktop/onboard-sdk-test/on-board-test/server/Onboard-SDK/sample/linux/missions/cmake-build-debug/CMakeFiles/djiosdk-mission-sample.dir/mission_sample.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../../../../osdk-core/api/inc"
  "../../../../osdk-core/utility/inc"
  "../../../../osdk-core/hal/inc"
  "../../../../osdk-core/protocol/inc"
  "../../../../osdk-core/platform/linux/inc"
  "../../common"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
