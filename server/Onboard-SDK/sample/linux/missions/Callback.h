//
// Created by ubuntu on 10/18/18.
//

#ifndef DJIOSDK_MISSION_SAMPLE_CALLBACK_H
#define DJIOSDK_MISSION_SAMPLE_CALLBACK_H

#include "mission_sample.hpp"
namespace Callback {

    void waypointEventCallback(DJI::OSDK::Vehicle *vehicle, DJI::OSDK::RecvContainer recvFrame,
                               DJI::OSDK::UserData userData);

};


#endif //DJIOSDK_MISSION_SAMPLE_CALLBACK_H
