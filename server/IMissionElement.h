//
// Created by ubuntu on 10/3/18.
//

#ifndef SERVER_IMISSIONELEMENT_H
#define SERVER_IMISSIONELEMENT_H

#include "MissionElement.h"
#include "constants/ErrorTypes.h"
#include <sstream>



class IMissionElement : public MissionElement {
public:
    IMissionElement();

    virtual void addPoint(pointStruct *ps);

    virtual void print();

    virtual std::string getType() = 0;

    virtual void init();

    virtual DJI::OSDK::ACK::ErrorCode pause();

    virtual DJI::OSDK::ACK::ErrorCode resume();

    virtual DJI::OSDK::ACK::ErrorCode stop();

    virtual DJI::OSDK::ACK::ErrorCode build();

    virtual DJI::OSDK::ACK::ErrorCode startMission();

    virtual std::string updateVelocity(float velocity);

    virtual std::vector<pointStruct> getPsVec();
};


#endif //SERVER_IMISSIONELEMENT_H
