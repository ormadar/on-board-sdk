# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.2

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/ubuntu/Downloads/pistache

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/ubuntu/Downloads/pistache/build

# Include any dependencies generated for this target.
include tests/CMakeFiles/run_mime_test.dir/depend.make

# Include the progress variables for this target.
include tests/CMakeFiles/run_mime_test.dir/progress.make

# Include the compile flags for this target's objects.
include tests/CMakeFiles/run_mime_test.dir/flags.make

tests/CMakeFiles/run_mime_test.dir/mime_test.cc.o: tests/CMakeFiles/run_mime_test.dir/flags.make
tests/CMakeFiles/run_mime_test.dir/mime_test.cc.o: ../tests/mime_test.cc
	$(CMAKE_COMMAND) -E cmake_progress_report /home/ubuntu/Downloads/pistache/build/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object tests/CMakeFiles/run_mime_test.dir/mime_test.cc.o"
	cd /home/ubuntu/Downloads/pistache/build/tests && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/run_mime_test.dir/mime_test.cc.o -c /home/ubuntu/Downloads/pistache/tests/mime_test.cc

tests/CMakeFiles/run_mime_test.dir/mime_test.cc.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/run_mime_test.dir/mime_test.cc.i"
	cd /home/ubuntu/Downloads/pistache/build/tests && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/ubuntu/Downloads/pistache/tests/mime_test.cc > CMakeFiles/run_mime_test.dir/mime_test.cc.i

tests/CMakeFiles/run_mime_test.dir/mime_test.cc.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/run_mime_test.dir/mime_test.cc.s"
	cd /home/ubuntu/Downloads/pistache/build/tests && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/ubuntu/Downloads/pistache/tests/mime_test.cc -o CMakeFiles/run_mime_test.dir/mime_test.cc.s

tests/CMakeFiles/run_mime_test.dir/mime_test.cc.o.requires:
.PHONY : tests/CMakeFiles/run_mime_test.dir/mime_test.cc.o.requires

tests/CMakeFiles/run_mime_test.dir/mime_test.cc.o.provides: tests/CMakeFiles/run_mime_test.dir/mime_test.cc.o.requires
	$(MAKE) -f tests/CMakeFiles/run_mime_test.dir/build.make tests/CMakeFiles/run_mime_test.dir/mime_test.cc.o.provides.build
.PHONY : tests/CMakeFiles/run_mime_test.dir/mime_test.cc.o.provides

tests/CMakeFiles/run_mime_test.dir/mime_test.cc.o.provides.build: tests/CMakeFiles/run_mime_test.dir/mime_test.cc.o

# Object files for target run_mime_test
run_mime_test_OBJECTS = \
"CMakeFiles/run_mime_test.dir/mime_test.cc.o"

# External object files for target run_mime_test
run_mime_test_EXTERNAL_OBJECTS =

tests/run_mime_test: tests/CMakeFiles/run_mime_test.dir/mime_test.cc.o
tests/run_mime_test: tests/CMakeFiles/run_mime_test.dir/build.make
tests/run_mime_test: googletest-release-1.7.0/libgtest.a
tests/run_mime_test: googletest-release-1.7.0/libgtest_main.a
tests/run_mime_test: src/libpistache.a
tests/run_mime_test: googletest-release-1.7.0/libgtest.a
tests/run_mime_test: tests/CMakeFiles/run_mime_test.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX executable run_mime_test"
	cd /home/ubuntu/Downloads/pistache/build/tests && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/run_mime_test.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
tests/CMakeFiles/run_mime_test.dir/build: tests/run_mime_test
.PHONY : tests/CMakeFiles/run_mime_test.dir/build

tests/CMakeFiles/run_mime_test.dir/requires: tests/CMakeFiles/run_mime_test.dir/mime_test.cc.o.requires
.PHONY : tests/CMakeFiles/run_mime_test.dir/requires

tests/CMakeFiles/run_mime_test.dir/clean:
	cd /home/ubuntu/Downloads/pistache/build/tests && $(CMAKE_COMMAND) -P CMakeFiles/run_mime_test.dir/cmake_clean.cmake
.PHONY : tests/CMakeFiles/run_mime_test.dir/clean

tests/CMakeFiles/run_mime_test.dir/depend:
	cd /home/ubuntu/Downloads/pistache/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/ubuntu/Downloads/pistache /home/ubuntu/Downloads/pistache/tests /home/ubuntu/Downloads/pistache/build /home/ubuntu/Downloads/pistache/build/tests /home/ubuntu/Downloads/pistache/build/tests/CMakeFiles/run_mime_test.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : tests/CMakeFiles/run_mime_test.dir/depend

