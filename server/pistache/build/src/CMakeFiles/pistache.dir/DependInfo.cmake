# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/ubuntu/Downloads/pistache/src/client/client.cc" "/home/ubuntu/Downloads/pistache/build/src/CMakeFiles/pistache.dir/client/client.cc.o"
  "/home/ubuntu/Downloads/pistache/src/common/cookie.cc" "/home/ubuntu/Downloads/pistache/build/src/CMakeFiles/pistache.dir/common/cookie.cc.o"
  "/home/ubuntu/Downloads/pistache/src/common/description.cc" "/home/ubuntu/Downloads/pistache/build/src/CMakeFiles/pistache.dir/common/description.cc.o"
  "/home/ubuntu/Downloads/pistache/src/common/http.cc" "/home/ubuntu/Downloads/pistache/build/src/CMakeFiles/pistache.dir/common/http.cc.o"
  "/home/ubuntu/Downloads/pistache/src/common/http_defs.cc" "/home/ubuntu/Downloads/pistache/build/src/CMakeFiles/pistache.dir/common/http_defs.cc.o"
  "/home/ubuntu/Downloads/pistache/src/common/http_header.cc" "/home/ubuntu/Downloads/pistache/build/src/CMakeFiles/pistache.dir/common/http_header.cc.o"
  "/home/ubuntu/Downloads/pistache/src/common/http_headers.cc" "/home/ubuntu/Downloads/pistache/build/src/CMakeFiles/pistache.dir/common/http_headers.cc.o"
  "/home/ubuntu/Downloads/pistache/src/common/mime.cc" "/home/ubuntu/Downloads/pistache/build/src/CMakeFiles/pistache.dir/common/mime.cc.o"
  "/home/ubuntu/Downloads/pistache/src/common/net.cc" "/home/ubuntu/Downloads/pistache/build/src/CMakeFiles/pistache.dir/common/net.cc.o"
  "/home/ubuntu/Downloads/pistache/src/common/os.cc" "/home/ubuntu/Downloads/pistache/build/src/CMakeFiles/pistache.dir/common/os.cc.o"
  "/home/ubuntu/Downloads/pistache/src/common/peer.cc" "/home/ubuntu/Downloads/pistache/build/src/CMakeFiles/pistache.dir/common/peer.cc.o"
  "/home/ubuntu/Downloads/pistache/src/common/reactor.cc" "/home/ubuntu/Downloads/pistache/build/src/CMakeFiles/pistache.dir/common/reactor.cc.o"
  "/home/ubuntu/Downloads/pistache/src/common/stream.cc" "/home/ubuntu/Downloads/pistache/build/src/CMakeFiles/pistache.dir/common/stream.cc.o"
  "/home/ubuntu/Downloads/pistache/src/common/tcp.cc" "/home/ubuntu/Downloads/pistache/build/src/CMakeFiles/pistache.dir/common/tcp.cc.o"
  "/home/ubuntu/Downloads/pistache/src/common/timer_pool.cc" "/home/ubuntu/Downloads/pistache/build/src/CMakeFiles/pistache.dir/common/timer_pool.cc.o"
  "/home/ubuntu/Downloads/pistache/src/common/transport.cc" "/home/ubuntu/Downloads/pistache/build/src/CMakeFiles/pistache.dir/common/transport.cc.o"
  "/home/ubuntu/Downloads/pistache/src/server/endpoint.cc" "/home/ubuntu/Downloads/pistache/build/src/CMakeFiles/pistache.dir/server/endpoint.cc.o"
  "/home/ubuntu/Downloads/pistache/src/server/listener.cc" "/home/ubuntu/Downloads/pistache/build/src/CMakeFiles/pistache.dir/server/listener.cc.o"
  "/home/ubuntu/Downloads/pistache/src/server/router.cc" "/home/ubuntu/Downloads/pistache/build/src/CMakeFiles/pistache.dir/server/router.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../include"
  "../src/../include"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
