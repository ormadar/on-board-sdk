# CMake generated Testfile for 
# Source directory: /home/ubuntu/Downloads/pistache
# Build directory: /home/ubuntu/Downloads/pistache/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(src)
subdirs(examples)
subdirs(googletest-release-1.7.0)
subdirs(tests)
