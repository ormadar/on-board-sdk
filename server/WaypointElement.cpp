//
// Created by ubuntu on 10/2/18.
//
#include "WaypointElement.h"
#include <iostream>
#include <functional>
#include <dji_vehicle_callback.hpp>

int responseTimeOut = 100;

void WaypointElement::updateCurrentMission(int waypointIndex, int parameter) {
    /* Rebuild the psVec struct with the new parameter */
    /* After that just run the mission normally */
}

void myCallbck(DJI::OSDK::Vehicle *vehicle, DJI::OSDK::RecvContainer recvFrame,
               DJI::OSDK::UserData userData) {
    std::cout << "\n my waypoint callback \n" << std::endl;
}

WaypointElement::WaypointElement(std::string type, DJI::OSDK::Vehicle *vehicle) {
    this->type = type;
    this->vehicle = vehicle;
}

void WaypointElement::setWaypointEventCallback(DJI::OSDK::VehicleCallBack callback) {
    this->vehicle->missionManager->wpMission->setWaypointEventCallback(callback, 0);
    this->vehicle->missionManager->wpMission->setWaypointCallback(callback, 0);
}

DJI::OSDK::ACK::ErrorCode WaypointElement::startMission() {
    DJI::OSDK::ACK::ErrorCode ackError;
    std::cout << "Starting waypoint mission..." << std::endl;
    ackError = this->vehicle->missionManager->wpMission->start(responseTimeOut);
    if (ACK::getError(ackError)) {
        std::cout << "Start error" << ackError.data << std::endl;
    } else {
        std::cout << "Start Good" << ackError.data << std::endl;
    }
    std::cout << "after start" << std::endl;
    std::cout << "CALLBACK!! : " << this->vehicle->missionManager->wpMission->wayPointEventCallback.callback
              << std::endl;
    setWaypointEventCallback(myCallbck);
    std::cout << "CALLBACK!! : " << this->vehicle->missionManager->wpMission->wayPointEventCallback.callback
              << std::endl;
    return ackError;
}

DJI::OSDK::ACK::ErrorCode WaypointElement::build() {
    DJI::OSDK::ACK::ErrorCode ackError;
    ackError = this->initStartSettings();
    if (ACK::getError(ackError)) {
        return ackError;
    }
    this->vehicle->missionManager->printInfo();
    uploadWaypointMission();
    return ackError;
}

DJI::OSDK::ACK::ErrorCode WaypointElement::initStartSettings() {
    std::cout << "Initializing waypoint mission..." << std::endl;
    DJI::OSDK::ACK::ErrorCode ackError;

    ackError = this->vehicle->missionManager->init(DJI::OSDK::DJI_MISSION_TYPE::WAYPOINT, responseTimeOut,
                                                   &this->initSettings);
    if (ACK::getError(ackError)) {
        std::cout << "start settings error" << std::endl;
    } else {
        std::cout << "start settings Good" << std::endl;
    }
    return ackError;

}

void WaypointElement::uploadWaypointMission() {
    std::cout << "Uploading waypoint mission..." << std::endl;

    for (std::vector<DJI::OSDK::WayPointSettings>::iterator wp = this->wpVector.begin();
         wp != this->wpVector.end(); ++wp) {
        DJI::OSDK::ACK::WayPointIndex wpDataACK = this->vehicle->missionManager->wpMission->uploadIndexData(&(*wp),
                                                                                                            responseTimeOut);
        std::cout << "wpDataACK: " << wpDataACK.ack.data << std::endl;
        if (DJI::OSDK::ACK::getError(wpDataACK.ack)) {
            const char *prob;
            DJI::OSDK::ACK::getErrorCodeMessage(wpDataACK.ack, prob);
            std::cout << "Error uploadding!!!! : " << prob << std::endl;

        } else {
            std::cout << "uploading success !!!" << std::endl;
        }
    }
}

DJI::OSDK::ACK::ErrorCode WaypointElement::pause() {
    DJI::OSDK::ACK::ErrorCode ackError;
    ackError = this->vehicle->missionManager->wpMission->pause(responseTimeOut);
    return ackError;
}

DJI::OSDK::ACK::ErrorCode WaypointElement::resume() {
    DJI::OSDK::ACK::ErrorCode ackError;
    ackError = this->vehicle->missionManager->wpMission->resume(responseTimeOut);
    return ackError;
}

DJI::OSDK::ACK::ErrorCode WaypointElement::stop() {
    DJI::OSDK::ACK::ErrorCode ackError;
    ackError = this->vehicle->missionManager->wpMission->stop(responseTimeOut);
    return ackError;
}

void WaypointElement::print() {

    if (this->psVec.size()) {
        for (int i = 0; i < this->psVec.size(); i++) {
            std::cout << "point number: " << i << "lat: " << psVec.at(i).lat << " alt: " << psVec.at(i).alt
                      << " lng :" << psVec.at(i).lng << " v: "
                      << psVec.at(i).v << " type: " << psVec.at(i).type << std::endl;
        }
    }

}

std::string WaypointElement::getType() {
    return this->type;
}


void WaypointElement::addPoint(pointStruct *ps) {
    this->psVec.push_back(*ps);
}

std::vector<DJI::OSDK::WayPointSettings> WaypointElement::getWaypointSettings() {
    return this->wpVector;
}

DJI::OSDK::WayPointInitSettings WaypointElement::getInitSettings() {
    return this->initSettings;
}

void WaypointElement::init() {
    int yawMode;
    std::cout << "init WaypointElement" << std::endl;
    if (this->psVec[0].headingType == "auto_mode") {
        yawMode = 0;
    } else {
        yawMode = 3;
    }
    setWaypointInitDefaults(yawMode);
    uint8_t numWaypoints = static_cast<uint8_t>(this->psVec.size());
    if (this->psVec.size() != 0) {
        this->initSettings.idleVelocity = this->psVec[0].v; // Settings first point speed
        this->initSettings.indexNumber = numWaypoints;
    } else {
        std::cout << "psVec has size = 0" << std::endl;
    }
    createWaypoints();
}

void WaypointElement::setWaypointInitDefaults(int yawMode) {
    uint8_t finishAction = 2;
    this->initSettings.maxVelocity = 15;
    this->initSettings.idleVelocity = 5;
    this->initSettings.finishAction = finishAction;
    this->initSettings.executiveTimes = 1;
    this->initSettings.yawMode = yawMode;
    this->initSettings.traceMode = 0;
    this->initSettings.RCLostAction = 1;
    this->initSettings.gimbalPitch = 0;
    this->initSettings.latitude = 0;
    this->initSettings.longitude = 0;
    this->initSettings.altitude = 0;
    std::cout << "After Waypoint Settings init" << std::endl;
}

void WaypointElement::createWaypoints() {
    int numOfWaypoints = this->psVec.size();
    for (int i = 0; i < numOfWaypoints; i++) {
        DJI::OSDK::WayPointSettings wp;
        setWaypointDefaults(&wp, this->psVec[i].heading);
        std::cout << "Point number: " << i << std::endl;
        wp.index = i;
        wp.latitude = this->psVec[i].lat;
        wp.longitude = this->psVec[i].lng;
        wp.altitude = this->psVec[i].alt;
        this->wpVector.push_back(wp);
    }
}

void WaypointElement::setWaypointDefaults(DJI::OSDK::WayPointSettings *wp, int yaw) {
    wp->damping = 0;
    wp->yaw = yaw;
    wp->gimbalPitch = 0;
    wp->turnMode = 0;
    wp->hasAction = 0;
    wp->actionTimeLimit = 100;
    wp->actionNumber = 0;
    wp->actionRepeat = 0;
    for (int i = 0; i < 16; ++i) {
        wp->commandList[i] = 0;
        wp->commandParameter[i] = 0;
    }
}

std::string WaypointElement::updateVelocity(float velocity) {
    std::stringstream returnJson;
    ACK::WayPointVelocity ackVelocity;
    std::cout << "Inside update velocity WAYPOINT" << std::endl;
    if (velocity > 15) {
        returnJson << "{msg:updateVelocity error, speed is too high:" << "}";
        return returnJson.str();
    }
    ackVelocity = this->vehicle->missionManager->wpMission->updateIdleVelocity(velocity, responseTimeOut);
    if (ACK::getError(ackVelocity.ack)) {
        returnJson << "{msg:updateVelocity error,data:" << ErrorTypes::getErrorById(ackVelocity.ack.data) << "}";
        ACK::getErrorCodeMessage(ackVelocity.ack, __func__);
        return returnJson.str();
    }
    returnJson << "{msg:updateVelocity success,data:" << ErrorTypes::getErrorById(ackVelocity.ack.data) << "}";
    return returnJson.str();
}

std::vector<pointStruct> WaypointElement::getPsVec() {
    return this->psVec;
}

