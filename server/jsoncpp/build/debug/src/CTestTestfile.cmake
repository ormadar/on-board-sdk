# CMake generated Testfile for 
# Source directory: /home/ubuntu/Downloads/jsoncpp-master/src
# Build directory: /home/ubuntu/Downloads/jsoncpp-master/build/debug/src
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(lib_json)
subdirs(jsontestrunner)
subdirs(test_lib_json)
