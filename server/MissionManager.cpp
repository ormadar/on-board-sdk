//
// Created by ubuntu on 10/7/18.
//
#include <vector>
#include <iostream>
#include "MissionManager.h"

MissionManagerRef *MissionManagerRef::_missionManager = NULL;

MissionManagerRef *MissionManagerRef::getInstance(DJI::OSDK::Vehicle **pvehicle) {
    if (!_missionManager) {
        _missionManager = new MissionManagerRef(pvehicle);
        return _missionManager;
    } else {
        return _missionManager;
    }
}

MissionManagerRef::MissionManagerRef(DJI::OSDK::Vehicle **pvehicle) {
    vehicle = *pvehicle;
    currentMissionIndex = 0;
    IMissionElement *element = NULL;
    std::vector<IMissionElement *> tempElementVec;
    tempElementVec.push_back(element);
    elementVec = tempElementVec;

}


void MissionManagerRef::setElementVector(std::vector<IMissionElement *> elementVector) {
    elementVec = elementVector;
}

DJI::OSDK::ACK::ErrorCode MissionManagerRef::pauseMission() {
    DJI::OSDK::ACK::ErrorCode ackError;
    if(elementVec[currentMissionIndex]) {
        ackError = elementVec[currentMissionIndex]->pause();
    }
    return ackError;
}

DJI::OSDK::ACK::ErrorCode MissionManagerRef::resumeMission() {
    DJI::OSDK::ACK::ErrorCode ackError;
    if(elementVec[currentMissionIndex]) {
        ackError = elementVec[currentMissionIndex]->resume();
    }
    return ackError;
}

DJI::OSDK::ACK::ErrorCode MissionManagerRef::stopMission() {
    DJI::OSDK::ACK::ErrorCode ackError;
    if(elementVec[currentMissionIndex]) {
        ackError = elementVec[currentMissionIndex]->stop();
        sleep(1);
    }
    return ackError;
}

DJI::OSDK::ACK::ErrorCode MissionManagerRef::startMission() {
    DJI::OSDK::ACK::ErrorCode ackError;
    ackError = elementVec[currentMissionIndex]->build();
    if (DJI::OSDK::ACK::getError(ackError) && ackError.data != 0) {
        DJI::OSDK::ACK::getErrorCodeMessage(ackError, "error building mission");
        std::cout << "Error building the mission" << std::endl;
        return ackError;
    }
    ackError = elementVec[currentMissionIndex]->startMission();
    if (DJI::OSDK::ACK::getError(ackError) && ackError.data != 0) {
        DJI::OSDK::ACK::getErrorCodeMessage(ackError, "error starting mission");
        std::cout << "Error starting the mission" << std::endl;;
        return ackError;
    }
    return ackError;
}

std::string MissionManagerRef::updateVelocity(float velocity) {
    std::string returnJson;
    returnJson = elementVec[currentMissionIndex]->updateVelocity(velocity);
    return returnJson;
}

IMissionElement* MissionManagerRef::getRunningElement()
{
    return elementVec[currentMissionIndex];
}
